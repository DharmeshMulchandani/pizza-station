from tkinter import *
import tkinter as tk
import os,os.path
from tkinter.font import Font
from tkinter import messagebox
import MySQLdb
import mysql.connector
import smtplib

root = Tk()

root.title("Pizza Station")
root.config(bg="#555353")
root.iconbitmap('images/pizza_logo.ico')

w = 1280
h = 660
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

x = (screen_width/2) - (w/2)
y = (screen_height/2) - (h/2)

root.geometry("%dx%d+%d+%d" % (w,h,x,y))
root.resizable(False,False)



#Custom Font
Troboto = Font(family="Roboto",
               size=40,
               weight="bold")

roboto = Font(family="Roboto",
               size=25)

vproboto = Font(family="Roboto",
               size=18)

PizzaTitle = Font(family="Roboto",
               size=30,
               weight="bold")

pizzaDes = Font(family="Roboto",
               size=15)
toppings = Font(family="Roboto",
                size=20,
                weight="bold")
toppingname = Font(family="Roboto",
               size=10)


img1 = PhotoImage(file='images/home2.png')
LImg = PhotoImage(file='images/login.png')


veg = PhotoImage(file='images/veg.png')
nonveg = PhotoImage(file='images/nonveg.png')

vegPImg = PhotoImage(file='images/vegpizza.png')
nonPImg = PhotoImage(file='images/nonvegpizza.png')
dessertImg = PhotoImage(file='images/dessert.png')

vpizza1Img = PhotoImage(file='images/cheesepizza1.png')
mvpizza1Img = PhotoImage(file='images/cheesepizzamain.png')

vpizza2Img = PhotoImage(file='images/tangytomato.png')
mvpizza2Img = PhotoImage(file='images/tangytomatocontainer.png')

vpizza3Img = PhotoImage(file='images/capcium.png')
mvpizza3Img = PhotoImage(file='images/capciumpizzacontainer.png')

vpizza4Img = PhotoImage(file='images/vegi.png')
mvpizza4Img = PhotoImage(file='images/vegicontainer.png')


vpizza5Img = PhotoImage(file='images/onionpizza.png')
mvpizza5Img = PhotoImage(file='images/onionpizzacontainer.png')

vpizza6Img = PhotoImage(file='images/mushroompizza.png')
mvpizza6Img = PhotoImage(file='images/mushroomcontainer.png')

vpizza7Img = PhotoImage(file='images/ExtraVegi.png')
mvpizza7Img = PhotoImage(file='images/ExtraVegicontainer.png')

vpizza8Img = PhotoImage(file='images/Supriem.png')
mvpizza8Img = PhotoImage(file='images/Supriemcontainer.png')


vpizza9Img = PhotoImage(file='images/pineapplepizza.png')
mvpizza9Img = PhotoImage(file='images/pineapplepizzacontainer.png')

vpizza10Img = PhotoImage(file='images/choclatepizza.png')
mvpizza10Img = PhotoImage(file='images/choclatepizzacontainer.png')

vpizza11Img = PhotoImage(file='images/healthy.png')
mvpizza11Img = PhotoImage(file='images/healthycontainer.png')



npizza1Img = PhotoImage(file='images/cheesychicken.png')
mnpizza1Img = PhotoImage(file='images/cheesychickencontainer.png')

npizza2Img = PhotoImage(file='images/chickenloaded.png')
mnpizza2Img = PhotoImage(file='images/chickenloadedcontainer.png')

npizza3Img = PhotoImage(file='images/pepperoni.png')
mnpizza3Img = PhotoImage(file='images/pepperonicontainer.png')

npizza4Img = PhotoImage(file='images/chickenmushroom.png')
mnpizza4Img = PhotoImage(file='images/chickenmushroomcontainer.png')


npizza5Img = PhotoImage(file='images/keema.png')
mnpizza5Img = PhotoImage(file='images/keemacontainer.png')

npizza6Img = PhotoImage(file='images/mixed.png')
mnpizza6Img = PhotoImage(file='images/mixedcontainer.png')

npizza7Img = PhotoImage(file='images/pepper.png')
mnpizza7Img = PhotoImage(file='images/peppercontainer.png')

npizza8Img = PhotoImage(file='images/supremechicken.png')
mnpizza8Img = PhotoImage(file='images/supremechickencontainer.png')

npizza9Img = PhotoImage(file='images/sweetchicken.png')
mnpizza9Img = PhotoImage(file='images/sweetchickencontainer.png')

npizza10Img = PhotoImage(file='images/spicy.png')
mnpizza10Img = PhotoImage(file='images/spicycontainer.png')

npizza11Img = PhotoImage(file='images/sausage.png')
mnpizza11Img = PhotoImage(file='images/sausagecontainer.png')

#---------------------------------------------------------------Dessert Images
dessertImg1 = PhotoImage(file='images/brownie.png')
mdessert1 = PhotoImage(file='images/BrowniesContainer.png')

dessertImg2 = PhotoImage(file='images/cupcakes1.png')
mdessert2 = PhotoImage(file='images/CupcakeContainer.png')

dessertImg3 = PhotoImage(file='images/donut.png')
mdessert3 = PhotoImage(file='images/DonutsContainer.png')

dessertImg4 = PhotoImage(file='images/pastry.png')
mdessert4 = PhotoImage(file='images/PastryContainer.png')

dessertImg5 = PhotoImage(file='images/pancakes.png')
mdessert5 = PhotoImage(file='images/PancakesConatiner.png')

dessertImg6 = PhotoImage(file='images/pie.png')
mdessert6 = PhotoImage(file='images/PieContainer.png')

dessertImg7 = PhotoImage(file='images/macarons.png')
mdessert7 = PhotoImage(file='images/MacaronsContainer.png')


vpizzabackImg = PhotoImage(file='images/back.png')

topping1 = PhotoImage(file='images/onion.png')
topping2 = PhotoImage(file='images/Tomato.png')
topping3 = PhotoImage(file='images/Capsicum.png')
topping4 = PhotoImage(file='images/corn.png')
topping5 = PhotoImage(file='images/mushroom.png')
topping6 = PhotoImage(file='images/pineapple.png')

sizeImg = PhotoImage(file='images/pizza1.png')

cart = PhotoImage(file='images/cart.png')
done = PhotoImage(file='images/done.png')

view = PhotoImage(file='images/view.png')

vegorder = PhotoImage(file='images/vegorder1.png')

line = PhotoImage(file='images/line.png')

remove = PhotoImage(file='images/remove.png')

doneImg = PhotoImage(file='images/right.png')

menuImg = PhotoImage(file='images/menu.png')

loimg = PhotoImage(file='images/logout.png')

flavour1Img = PhotoImage(file='images/flavour1.png')
flavour2Img = PhotoImage(file='images/flavour2.png')
flavour3Img = PhotoImage(file='images/flavour3.png')
flavour4Img = PhotoImage(file='images/flavour4.png')

Bpack2 = PhotoImage(file='images/B2P.png')
Bpack4 = PhotoImage(file='images/B4P.png')
Bpack6 = PhotoImage(file='images/B6P.png')
Bpack1 = PhotoImage(file='images/B1P.png')
Bpack3 = PhotoImage(file='images/B3P.png')
Bpack8 = PhotoImage(file='images/B8P.png')
Bpack12 = PhotoImage(file='images/B12P.png')

class PizzaSystem:
    def main():             
        def login():
            mb.destroy()
            start.destroy()

            Lbg = Button(root,
                         image=LImg,
                         height=660,
                         width=1280,
                         relief=SOLID,
                         bd=0
                         )
            Lbg.pack()

            LTitle = Label(root,text="Sign In",font=Troboto,bg="#000000",fg="#ffffff")
            LTitle.place(x=860,y=130)

            leid = Label(root,text="Employee ID",font=roboto,bg="#000000",fg="#ffffff")
            leid.place(x=830,y=230)

            le1 = Entry(root,bg="#000000",fg="#ffffff",width=25,font=("Calibri",15),relief=GROOVE,bd=4,insertbackground="#ffffff")
            le1.place(x=833,y=280)

            le1.focus()

            lpwd = Label(root,text="Password",font=roboto,bg="black",fg="#ffffff")
            lpwd.place(x=830,y=340)

            le2 = Entry(root,bg="#000000",fg="#ffffff",width=25,font=("Calibri",15),relief=GROOVE,bd=4,insertbackground="#ffffff",show="*")
            le2.place(x=833,y=390)


            def menu():
                Lbg.destroy()
                LTitle.destroy()
                leid.destroy()
                le1.destroy()
                lpwd.destroy()
                le2.destroy()
                lsubmit.destroy()
                global t

                frame = Frame(root,height=220,width=1250,bg="#555353")
                frame.place(x=15,y=420)

                size = Frame(root, height=200, width=680,bg="#555353")
                size.place(x=560,y=280)

                detail = Frame(root, height=150,width=500,bg="#555353")
                detail.place(x=5,y=280)


                def insertPizza(name):
                    global j
                    pizza1=name
                    dbname=MySQLdb.Connect("localhost","root","","pizza")
                    c2=dbname.cursor()
                    try:
                        chkq="select pno from pizzadetail"
                        c2.execute(chkq)
                        collect=c2.fetchall()
                        dummy=0
                        convert=str(dummy)
                        ddata=[]
                        for i in collect:
                            pass
                        for j in i:
                            if j<=j:
                                j=j+1
                        insname="insert into pizzadetail values(%s,'"+ pizza1 +"','"+ convert +"','"+ convert +"','"+ convert +"')"
                        no_data=(j,)
                        c2.execute(insname,no_data)
                        dbname.commit()
                        messagebox.showinfo("You have selected",pizza1) 
                    except Exception as ex:
                        dbname.rollback()
                        messagebox.showinfo("Pizza Station","Error")
                        print(ex)
                    dbname.close()

                def insertSize(pizza_size):
                    dbsize = MySQLdb.connect("localhost","root","","pizza")
                    c_size = dbsize.cursor()
                    try:
                        q_size = "update pizzadetail set psize=%s where pno=%s"
                        data=(pizza_size,j)
                        c_size.execute(q_size,data)
                        dbsize.commit()
                        messagebox.showinfo("Pizza Station","Selected")
                    except Exception as ex:
                        dbsize.rollback()
                        messagebox.showinfo("Pizza Station","Error")
                        print(ex)
                    dbsize.close()


                def qtyFunc():
                    def value():
                        cv=qty_sb.get()
                        dbqty = MySQLdb.connect("localhost","root","","pizza")
                        c_qty = dbqty.cursor()
                        try:
                            q_qty = "update pizzadetail set qty=%s where pno=%s"
                            qty_data=(cv,j)
                            c_qty.execute(q_qty,qty_data)
                            dbqty.commit()
                        except Exception as ex:
                            dbqty.rollback()
                            messagebox.showerror("Pizza Station","Error!!")
                            print(ex)
                        dbqty.close()
                                  
                    qty_name = Label(size,text="Quantity",bg="#555353",fg="#ffffff",font=toppings)
                    qty_name.place(x=510,y=30)

                    qty_sb = Spinbox(size, from_=0, to = 5,bg="#ffffff",fg="#000000",command=value,font=Font(family='Helvetica', size=28, weight='bold'),width=5)
                    qty_sb.place(x=510,y=70)

                    

                def toppingsFunc():
                    def insertToppings(t):
                        dbtopping = MySQLdb.connect("localhost","root","","pizza")
                        c_topping = dbtopping.cursor()
                        try:
                            q_topping = "update pizzadetail set topping=%s where pno=%s"
                            data=(t,j)
                            c_topping.execute(q_topping,data)
                            dbtopping.commit()
                            messagebox.showinfo("Pizza Station","Selected")
                        except Exception as ex:
                            dbtopping.rollback()
                            messagebox.showerror("Pizza Station","Error")
                            print(ex)
                        dbtopping.close()

                    vegLogo1 = Button(detail,
                                      relief=FLAT,
                                      image=veg,
                                      width=35,     #veg logo
                                      height=35,
                                      bg="#555353"
                                      )
                    vegLogo1.place(x=25,y=10)

                    vpizza1Size = Label(size,text="Size",font=toppings,bg="#555353",fg="#ffffff")    #Size Text
                    vpizza1Size.place(x=5,y=2)

                    vpizzatoppings = Label(frame,
                                           text="Toppings Rs 30/-",
                                           bg="#555353",
                                           fg="#ffffff",
                                           font=toppings
                                           )
                    vpizzatoppings.place(x=15,y=20)
                        
                    oniontopping = Button(frame,
                                          text="Onion",
                                          bg="#000000",
                                          fg="#ffffff",
                                          height=115,
                                          width=100,
                                          image=topping1,
                                          compound=TOP,
                                          pady=8,
                                          font=pizzaDes,
                                          command=lambda t=35: insertToppings(t))
                    oniontopping.place(x=5,y=70)

                    tomatotopping = Button(frame,
                                          text="Tomato",
                                          bg="#000000",
                                          fg="#ffffff",
                                          height=115,
                                          width=100,
                                          image=topping2,
                                          compound=TOP,
                                          pady=8,
                                          font=pizzaDes,
                                           command=lambda t=35: insertToppings(t))
                    tomatotopping.place(x=140,y=70)

                    capsicumtopping = Button(frame,
                                             text="Capsicum",
                                             bg="#000000",
                                             fg="#ffffff",
                                             height=115,
                                             width=100,
                                             image=topping3,
                                             compound=TOP,
                                             pady=8,
                                             font=pizzaDes,
                                             command=lambda t=35: insertToppings(t))
                    capsicumtopping.place(x=275,y=70)

                    corntopping = Button(frame,
                                          text="Corn",
                                          bg="#000000",
                                          fg="#ffffff",
                                          height=115,
                                          width=100,
                                          image=topping4,
                                          compound=TOP,
                                          pady=8,
                                          font=pizzaDes,
                                         command=lambda t=35: insertToppings(t))
                    corntopping.place(x=410,y=70)

                    mushroomtopping = Button(frame,
                                              text="Mushroom",
                                              bg="#000000",
                                              fg="#ffffff",
                                              height=115,
                                              width=100,
                                              image=topping5,
                                              compound=TOP,
                                              pady=8,
                                              font=pizzaDes,
                                             command=lambda t=35: insertToppings(t))
                    mushroomtopping.place(x=545,y=70)

                    pineappletopping = Button(frame,
                                              text="Pineapple",
                                              bg="#000000",
                                              fg="#ffffff",
                                              height=115,
                                              width=100,
                                              image=topping6,
                                              compound=TOP,
                                              pady=8,
                                              font=pizzaDes,
                                              command=lambda t=35: insertToppings(t))
                    pineappletopping.place(x=680,y=70)

                    def change():
                        addToCart.config(bg="#21E171",image=done,text="Done",padx=5,pady=5,width=160)

                    addToCart = Button(frame,
                                       text="Add To cart",
                                       image=cart,
                                       compound=LEFT,
                                       relief=RAISED,
                                       font=vproboto,
                                       fg="#ffffff",
                                       bg="#6C9BEA",
                                       padx=5,
                                       pady=5,
                                       command=change
                                       )
                    addToCart.place(x=950,y=80)

                def nonvegLogo():
                    nonvegLogo1 = Button(detail,
                                         relief=FLAT,
                                         image=nonveg,
                                         width=35,
                                         height=35,
                                         bg="#555353"
                                         )
                    nonvegLogo1.place(x=25,y=10)

                def vegPizza():
                    vegPizzaBtn.destroy()
                    nonPizzaBtn.destroy()
                    dessertBtn.destroy()
                    viewCart.destroy()
                    logout.destroy()
                    
                    mainframe = Frame(root,height=660,width=1280,bg="#555353")
                    mainframe.pack()

                    def cheesePizza(name):
                        insertPizza(name)
                        toppingsFunc()
                        mainframe.destroy()
                        

                        topContainer = Button(root,
                                              relief=FLAT,
                                              image=mvpizza1Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer.place(x=0,y=0)


                        vpizza1name = Label(detail,
                                            text="Cheese Pizza",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza1name.place(x=70,y=5)

                        vpizza1detail = Label(detail,
                                              text="Loaded with cheese and italian sauce with hand made crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail.place(x=25,y=55)

                        vpizzaprice = Label(detail,
                                            text="Rs 400/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice.place(x=25,y=115)
                        
            
                        sizeL = Button(size,
                                       text="Large 600/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=600: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 400/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=400: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 200/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=200: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()
                        

                        def cheesePizzaBack():
                            topContainer.destroy()
                            cheesepizzaback.destroy()
                            vpizza1name.destroy()
                            vpizza1detail.destroy()
                            vpizzaprice.destroy()
                            vegPizza()
                            

                        cheesepizzaback = Button(frame,
                                                 text="Back",
                                                 command=cheesePizzaBack,
                                                 image=vpizzabackImg,
                                                 compound=LEFT,
                                                 bg="#ffffff",
                                                 fg="#000000",
                                                 font=vproboto,
                                                 relief=RAISED,
                                                 padx=5,
                                                 pady=5,
                                                 width=160
                                                 )
                        cheesepizzaback.place(x=950,y=145)              


                    vpizza1 = Button(mainframe,
                                     text="Cheese Pizza",
                                     width=285,
                                     height=185,
                                     image=vpizza1Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Cheese Pizza": cheesePizza(d)
                                     )
                    vpizza1.place(x=30,y=10)

##--------------------------------------------------Cheese Pizza Ends here

                    def tangyTomato(name):
                        insertPizza(name)
                        mainframe.destroy()
                        toppingsFunc()
                        

                        topContainer2 = Button(root,
                                              relief=FLAT,
                                              image=mvpizza2Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer2.place(x=0,y=0)
                    
                        
                        vpizza2name = Label(detail,
                                            text="Tangy Tomato",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza2name.place(x=70,y=5)

                        vpizza1detail2 = Label(detail,
                                              text="Loaded with fresh farm tomatoes and tangy sauce with wheat crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail2.place(x=25,y=55)

                        vpizzaprice2 = Label(detail,
                                            text="Rs 470/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice2.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 777/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=777: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 470/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=470: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 300/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=300: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def tomatoback():
                            topContainer2.destroy()
                            tomatoback.destroy()
                            vpizza2name.destroy()
                            vpizza1detail2.destroy()
                            vpizzaprice2.destroy()
                            vegPizza()

                        tomatoback = Button(frame,
                                            text="Back",
                                            command=tomatoback,
                                            image=vpizzabackImg,
                                            compound=LEFT,
                                            bg="#ffffff",
                                            fg="#000000",
                                            font=vproboto,
                                            relief=RAISED,
                                            padx=5,
                                            pady=5,
                                            width=160
                                            )
                        tomatoback.place(x=950,y=145)
                        
                    
                    vpizza2 = Button(mainframe,
                                     text="Tangy Tomato",
                                     width=285,
                                     height=185,
                                     image=vpizza2Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Tangy Tomato": tangyTomato(d)
                                     )
                    vpizza2.place(x=340,y=10)

##--------------------------------------------------Tangy Tomato Pizza Ends here

                    def capcicumPizza(name):
                        insertPizza(name)
                        mainframe.destroy()
                        toppingsFunc()
                        

                        topContainer3 = Button(root,
                                              relief=FLAT,
                                              image=mvpizza3Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer3.place(x=0,y=0)
                    
                        
                        vpizza3name = Label(detail,
                                            text="Capcicum Loaded",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza3name.place(x=70,y=5)

                        vpizza1detail3 = Label(detail,
                                              text="Loaded with fresh Capcicum and spicy sauce with crunchy crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail3.place(x=25,y=55)

                        vpizzaprice3 = Label(detail,
                                            text="Rs 480/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice3.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 800/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=800: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 480/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=480: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 320/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=320: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def capcicumbackFunc():
                            topContainer3.destroy()
                            capcicumback.destroy()
                            vpizza3name.destroy()
                            vpizza1detail3.destroy()
                            vpizzaprice3.destroy()
                            vegPizza()

                        capcicumback = Button(frame,
                                            text="Back",
                                            command=capcicumbackFunc,
                                            image=vpizzabackImg,
                                            compound=LEFT,
                                            bg="#ffffff",
                                            fg="#000000",
                                            font=vproboto,
                                            relief=RAISED,
                                            padx=5,
                                            pady=5,
                                            width=160
                                            )
                        capcicumback.place(x=950,y=145)

                        
                    

                    vpizza3 = Button(mainframe,
                                     text="Capcicum Loaded",
                                     width=285,
                                     height=185,
                                     image=vpizza3Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Capcicum Loaded": capcicumPizza(d)
                                     )
                    vpizza3.place(x=650,y=10)

##--------------------------------------------------Capcicum Loaded Pizza Ends here

                    def vegiPizza(name):
                        insertPizza(name)
                        mainframe.destroy()
                        toppingsFunc()
                        

                        topContainer4 = Button(root,
                                              relief=FLAT,
                                              image=mvpizza4Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer4.place(x=0,y=0)
                    
                        
                        vpizza4name = Label(detail,
                                            text="Vegi",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza4name.place(x=70,y=5)

                        vpizza1detail4 = Label(detail,
                                              text="Loaded with fresh vegitables onion, capcicum, tomato and italian sauce with hand made crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail4.place(x=25,y=55)

                        vpizzaprice4 = Label(detail,
                                            text="Rs 550/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice4.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 900/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=900: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 550/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=550: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 400/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=400: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def vegibackFunc():
                            topContainer4.destroy()
                            vpizza4name.destroy()
                            vpizza1detail4.destroy()
                            vpizzaprice4.destroy()
                            vegiback.destroy()
                            vegPizza()

                        vegiback = Button(frame,
                                            text="Back",
                                            command=vegibackFunc,
                                            image=vpizzabackImg,
                                            compound=LEFT,
                                            bg="#ffffff",
                                            fg="#000000",
                                            font=vproboto,
                                            relief=RAISED,
                                            padx=5,
                                            pady=5,
                                            width=160
                                            )
                        vegiback.place(x=950,y=145)
                        

                    vpizza4 = Button(mainframe,
                                     text="Vegi",
                                     width=285,
                                     height=185,
                                     image=vpizza4Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Vegi": vegiPizza(d)
                                     )
                    vpizza4.place(x=960,y=10)

##--------------------------------------------------Vegi Pizza Ends here

                    def onionPizza(name):
                        insertPizza(name)
                        mainframe.destroy()
                        toppingsFunc()
                        

                        topContainer5 = Button(root,
                                              relief=FLAT,
                                              image=mvpizza5Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer5.place(x=0,y=0)
                    
                        
                        vpizza5name = Label(detail,
                                            text="Onion Loaded",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza5name.place(x=70,y=5)

                        vpizza1detail5 = Label(detail,
                                              text="Loaded with fresh Onion and spicy sauce with crunchy crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail5.place(x=25,y=55)

                        vpizzaprice5 = Label(detail,
                                            text="Rs 480/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice5.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 800/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=800: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 480/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=480: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 320/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=320: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def onionFunc():
                            topContainer5.destroy()
                            vpizza5name.destroy()
                            vpizza1detail5.destroy()
                            vpizzaprice5.destroy()
                            onionback.destroy()
                            vegPizza()

                        onionback = Button(frame,
                                            text="Back",
                                            command=onionFunc,
                                            image=vpizzabackImg,
                                            compound=LEFT,
                                            bg="#ffffff",
                                            fg="#000000",
                                            font=vproboto,
                                            relief=RAISED,
                                            padx=5,
                                            pady=5,
                                            width=160
                                            )
                        onionback.place(x=950,y=145)


                    vpizza5 = Button(mainframe,
                                     text="Onion Loaded",
                                     width=285,
                                     height=185,
                                     image=vpizza5Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Onion Loaded": onionPizza(d)
                                     )
                    vpizza5.place(x=30,y=220)

##--------------------------------------------------Onion Loaded Pizza Ends here

                    def mushroomPizza(name):
                        insertPizza(name)
                        mainframe.destroy()
                        toppingsFunc()
                        

                        topContainer6 = Button(root,
                                              relief=FLAT,
                                              image=mvpizza6Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer6.place(x=0,y=0)
                    
                        
                        vpizza6name = Label(detail,
                                            text="Mushroom Loaded",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza6name.place(x=70,y=5)

                        vpizza1detail6 = Label(detail,
                                              text="Loaded with fresh Mushroom and italian sauce with hand made crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail6.place(x=25,y=55)

                        vpizzaprice6 = Label(detail,
                                            text="Rs 500/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice6.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 820/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=800: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 500/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=480: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 350/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=320: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def mushroomFunc():
                            topContainer6.destroy()
                            mushroomback.destroy()
                            vpizza6name.destroy()
                            vpizza1detail6.destroy()
                            vpizzaprice6.destroy()
                            vegPizza()

                        mushroomback = Button(frame,
                                            text="Back",
                                            command=mushroomFunc,
                                            image=vpizzabackImg,
                                            compound=LEFT,
                                            bg="#ffffff",
                                            fg="#000000",
                                            font=vproboto,
                                            relief=RAISED,
                                            padx=5,
                                            pady=5,
                                            width=160
                                            )
                        mushroomback.place(x=950,y=145)

                    vpizza6 = Button(mainframe,
                                     text="Mushroom",
                                     width=285,
                                     height=185,
                                     image=vpizza6Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Mushroom": mushroomPizza(d)
                                     )
                    vpizza6.place(x=340,y=220)

##--------------------------------------------------Mushroom Loaded Pizza Ends here

                    def ExtraVegiPizza(name):
                        insertPizza(name)
                        mainframe.destroy()
                        toppingsFunc()
                        

                        topContainer7 = Button(root,
                                              relief=FLAT,
                                              image=mvpizza7Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer7.place(x=0,y=0)
                    
                        
                        vpizza7name = Label(detail,
                                            text="Extra Vegi",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza7name.place(x=70,y=5)

                        vpizza1detail7 = Label(detail,
                                              text="The vegis freshly comes from farm and specail sauce with soft crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail7.place(x=25,y=55)

                        vpizzaprice7 = Label(detail,
                                            text="Rs 666/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice7.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 970/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=970: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 666/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=666: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 450/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=450: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def extraVegiFunc():
                            topContainer7.destroy()
                            vpizza7name.destroy()
                            vpizza1detail7.destroy()
                            vpizzaprice7.destroy()
                            extraVegiback.destroy()
                            vegPizza()

                        extraVegiback = Button(frame,
                                            text="Back",
                                            command=extraVegiFunc,
                                            image=vpizzabackImg,
                                            compound=LEFT,
                                            bg="#ffffff",
                                            fg="#000000",
                                            font=vproboto,
                                            relief=RAISED,
                                            padx=5,
                                            pady=5,
                                            width=160
                                            )
                        extraVegiback.place(x=950,y=145)

                    vpizza7 = Button(mainframe,
                                     text="Extra Vegi",
                                     width=285,
                                     height=185,
                                     image=vpizza7Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Extra Vegi": ExtraVegiPizza(d)
                                     )
                    vpizza7.place(x=650,y=220)

##--------------------------------------------------Extra vegi Pizza Ends here

                    def SupriemPizza(name):
                        insertPizza(name)
                        mainframe.destroy()
                        toppingsFunc()
                        

                        topContainer8 = Button(root,
                                              relief=FLAT,
                                              image=mvpizza8Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer8.place(x=0,y=0)
                    
                        
                        vpizza8name = Label(detail,
                                            text="Supriem",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza8name.place(x=70,y=5)

                        vpizza1detail8 = Label(detail,
                                              text="Supriem of all the veg pizzas loaded with all vegis with specail italian sauce with cheesy crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail8.place(x=25,y=55)

                        vpizzaprice8 = Label(detail,
                                            text="Rs 800/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice8.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 1200/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=120: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 800/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=800: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 650/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=650: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def supriemFunc():
                            topContainer8.destroy()
                            vpizza8name.destroy()
                            vpizza1detail8.destroy()
                            vpizzaprice8.destroy()
                            supriemback.destroy()
                            size.destroy()
                            vegPizza()

                        supriemback = Button(frame,
                                            text="Back",
                                            command=supriemFunc,
                                            image=vpizzabackImg,
                                            compound=LEFT,
                                            bg="#ffffff",
                                            fg="#000000",
                                            font=vproboto,
                                            relief=RAISED,
                                            padx=5,
                                            pady=5,
                                            width=160
                                            )
                        supriemback.place(x=950,y=145)

                    vpizza8 = Button(mainframe,
                                     text="Supriem",
                                     width=285,
                                     height=185,
                                     image=vpizza8Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Supriem": SupriemPizza(d)
                                     )
                    vpizza8.place(x=960,y=220)


##--------------------------------------------------Supriem Pizza Ends here

                    def PineapplePizza(name):
                        insertPizza(name)
                        mainframe.destroy()
                        toppingsFunc()
                        

                        topContainer9 = Button(root,
                                              relief=FLAT,
                                              image=mvpizza9Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer9.place(x=0,y=0)
                    
                        
                        vpizza9name = Label(detail,
                                            text="Pineapple",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza9name.place(x=70,y=5)

                        vpizza1detail9 = Label(detail,
                                              text="Loaded with sweetest pineapple with spicy sauce and crushed pineapple crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail9.place(x=25,y=55)

                        vpizzaprice9 = Label(detail,
                                            text="Rs 555/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice9.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 900/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=900: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 555/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=555: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 380/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=380: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def pineappleFunc():
                            topContainer9.destroy()
                            vpizza9name.destroy()
                            vpizza1detail9.destroy()
                            vpizzaprice9.destroy()
                            pineappleback.destroy()
                            vegPizza()

                        pineappleback = Button(frame,
                                            text="Back",
                                            command=pineappleFunc,
                                            image=vpizzabackImg,
                                            compound=LEFT,
                                            bg="#ffffff",
                                            fg="#000000",
                                            font=vproboto,
                                            relief=RAISED,
                                            padx=5,
                                            pady=5,
                                            width=160
                                            )
                        pineappleback.place(x=950,y=145)

                    vpizza9 = Button(mainframe,
                                     text="Pineapple",
                                     width=285,
                                     height=185,
                                     image=vpizza9Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Pineapple Pizza": PineapplePizza(d)
                                     )
                    vpizza9.place(x=170,y=430)

##--------------------------------------------------Pineapple Loaded Pizza Ends here

                    def ChoclatyPizza(name):
                        insertPizza(name)
                        mainframe.destroy()
                        toppingsFunc()
                        

                        topContainer10 = Button(root,
                                              relief=FLAT,
                                              image=mvpizza10Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer10.place(x=0,y=0)
                    
                        
                        vpizza10name = Label(detail,
                                            text="Choclaty",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza10name.place(x=70,y=5)

                        vpizza1detail10 = Label(detail,
                                              text="Fully added with all three choclates milk, white & dark with specially mixed choclate sauce",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail10.place(x=25,y=55)

                        vpizzaprice10 = Label(detail,
                                            text="Rs 450/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice10.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 789/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=789: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 450/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=450: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 300/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=300: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def choclatyFunc():
                            topContainer10.destroy()
                            choclatyback.destroy()
                            vpizza10name.destroy()
                            vpizza1detail10.destroy()
                            vpizzaprice10.destroy()
                            vegPizza()

                        choclatyback = Button(frame,
                                            text="Back",
                                            command=choclatyFunc,
                                            image=vpizzabackImg,
                                            compound=LEFT,
                                            bg="#ffffff",
                                            fg="#000000",
                                            font=vproboto,
                                            relief=RAISED,
                                            padx=5,
                                            pady=5,
                                            width=160
                                            )
                        choclatyback.place(x=950,y=145)

                    vpizza10 = Button(mainframe,
                                     text="The Chocolaty",
                                     width=285,
                                     height=185,
                                     image=vpizza10Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                      command=lambda d="Choclaty":ChoclatyPizza(d)
                                     )
                    vpizza10.place(x=480,y=430)

##--------------------------------------------------choclaty Pizza Ends here

                    def TheHealthyPizza(name):
                        insertPizza(name)
                        mainframe.destroy()
                        toppingsFunc()
                        

                        topContainer11 = Button(root,
                                              relief=FLAT,
                                              image=mvpizza11Img,
                                              width=1285,
                                              height=260,
                                              bd=0)
                        topContainer11.place(x=0,y=0)
                    
                        
                        vpizza11name = Label(detail,
                                            text="The Healthy",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        vpizza11name.place(x=70,y=5)

                        vpizza1detail11 = Label(detail,
                                              text="Organic vegies with organic sauce made with wheat crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        vpizza1detail11.place(x=25,y=55)

                        vpizzaprice11 = Label(detail,
                                            text="Rs 750/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        vpizzaprice11.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 1100/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=110: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 750/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=750: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 600/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=600: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def thehealthyFunc():
                            topContainer11.destroy()
                            thehealthyback.destroy()
                            vpizza11name.destroy()
                            vpizza1detail11.destroy()
                            vpizzaprice11.destroy()
                            vegPizza()

                        thehealthyback = Button(frame,
                                            text="Back",
                                            command=thehealthyFunc,
                                            image=vpizzabackImg,
                                            compound=LEFT,
                                            bg="#ffffff",
                                            fg="#000000",
                                            font=vproboto,
                                            relief=RAISED,
                                            padx=5,
                                            pady=5,
                                            width=160
                                            )
                        thehealthyback.place(x=950,y=145)

                    vpizza11 = Button(mainframe,
                                     text="The Healthy",
                                     width=285,
                                     height=185,
                                     image=vpizza11Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                      command=lambda d="The Healthy":TheHealthyPizza(d)
                                     )
                    vpizza11.place(x=790,y=430)

                    def vPizzaBackFunc():
                        mainframe.destroy()
                        menu()

                    vpizzaback = Button(mainframe,
                                        text="Back",
                                        fg="#000000",
                                        bg="#ffffff",
                                        font=vproboto,
                                        image=vpizzabackImg,
                                        compound=LEFT,
                                        padx=10,
                                        command=vPizzaBackFunc
                                        )
                    vpizzaback.place(x=20,y=570)

                    
                    
#---------------------------------------------------------------------Veg Pizzas Ends here

#------------------------------------------------------------------NonVeg Pizza Starts----------------------------------------------------------------------

                def nonVeg():
                    vegPizzaBtn.destroy()
                    nonPizzaBtn.destroy()
                    dessertBtn.destroy()
                    viewCart.destroy()
                    logout.destroy()

                    nonvegFrame = Frame(root,width=1280,height=660,bg="#555353")
                    nonvegFrame.place(x=0,y=0)

                    def CheesyChicken(name):
                        insertPizza(name)
                        nonvegFrame.destroy()
                        toppingsFunc()
                        nonvegLogo()
                        

                        ntopContainer1 = Button(root,
                                                relief=FLAT,
                                                image=mnpizza1Img,
                                                width=1285,
                                                height=260,
                                                bd=0)
                        ntopContainer1.place(x=0,y=0)
                    
                        
                        npizza1name = Label(detail,
                                            text="Cheesy Chicken",
                                            font=PizzaTitle,
                                            bg="#555353",
                                            fg="#ffffff"
                                            )
                        npizza1name.place(x=70,y=5)

                        npizza1detail = Label(detail,
                                              text="Loaded with cheese and chicken with spicy sauce and cheesy crust",
                                              font=pizzaDes,
                                              bg="#555353",
                                              fg="#ffffff",
                                              wraplength=450,
                                              justify=LEFT
                                              )
                        npizza1detail.place(x=25,y=55)

                        npizzaprice1 = Label(detail,
                                            text="Rs 500/-",
                                            bg="#555353",
                                            fg="#ffffff",
                                            font=vproboto,
                                            justify=LEFT,
                                            )
                        npizzaprice1.place(x=25,y=115)

                        sizeL = Button(size,
                                       text="Large 700/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=700: insertSize(s))
                        sizeL.place(x=2,y=40)

                        sizeM = Button(size,
                                       text="Medium 500/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=500: insertSize(s))
                        sizeM.place(x=152,y=40)

                        sizeS = Button(size,
                                       text="Small 300/-",
                                       bg="#000000",
                                       fg="#ffffff",
                                       image=sizeImg,
                                       font=pizzaDes,
                                       compound=TOP,
                                       height=115,
                                       width=120,
                                       relief=FLAT,
                                       pady=8,
                                       command=lambda s=300: insertSize(s))
                        sizeS.place(x=302,y=40)

                        qtyFunc()

                        def cheesyChickenFunc():
                            ntopContainer1.destroy()
                            npizza1name.destroy()
                            npizza1detail.destroy()
                            npizzaprice1.destroy()
                            cheesychickenback.destroy()
                            nonVeg()

                        cheesychickenback = Button(frame,
                                                    text="Back",
                                                    command=cheesyChickenFunc,
                                                    image=vpizzabackImg,
                                                    compound=LEFT,
                                                    bg="#ffffff",
                                                    fg="#000000",
                                                    font=vproboto,
                                                    relief=RAISED,
                                                    padx=5,
                                                    pady=5,
                                                    width=160
                                                    )
                        cheesychickenback.place(x=950,y=145)

                
                        
                    npizza1 = Button(nonvegFrame,
                                     text="Cheesy Chicken",
                                     width=285,
                                     height=185,
                                     image=npizza1Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command= lambda d="Cheesy Chicken": CheesyChicken(d) 
                                     )
                    npizza1.place(x=30,y=10)
#--------------------------------------------------------Cheesy Chicken Pizza ends here

                    def ChickenLoaded(name):
                            insertPizza(name)
                            nonvegFrame.destroy()
                            toppingsFunc()
                            nonvegLogo()
                            

                            ntopContainer1 = Button(root,
                                                    relief=FLAT,
                                                    image=mnpizza2Img,
                                                    width=1285,
                                                    height=260,
                                                    bd=0)
                            ntopContainer1.place(x=0,y=0)
                        
                            
                            npizza1name = Label(detail,
                                                text="Chicken Loaded",
                                                font=PizzaTitle,
                                                bg="#555353",
                                                fg="#ffffff"
                                                )
                            npizza1name.place(x=70,y=5)

                            npizza1detail = Label(detail,
                                                  text="Fully Loaded with chicken and spicy indian sauce with crunchy crust",
                                                  font=pizzaDes,
                                                  bg="#555353",
                                                  fg="#ffffff",
                                                  wraplength=450,
                                                  justify=LEFT
                                                  )
                            npizza1detail.place(x=25,y=55)

                            npizzaprice1 = Label(detail,
                                                text="Rs 700/-",
                                                bg="#555353",
                                                fg="#ffffff",
                                                font=vproboto,
                                                justify=LEFT,
                                                )
                            npizzaprice1.place(x=25,y=115)

                            sizeL = Button(size,
                                           text="Large 1100/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=1100: insertSize(s))
                            sizeL.place(x=2,y=40)

                            sizeM = Button(size,
                                           text="Medium 700/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=700: insertSize(s))
                            sizeM.place(x=152,y=40)

                            sizeS = Button(size,
                                           text="Small 450/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=450: insertSize(s))
                            sizeS.place(x=302,y=40)

                            qtyFunc()

                            def chickenLoadedFunc():
                                ntopContainer1.destroy()
                                npizza1name.destroy()
                                npizza1detail.destroy()
                                npizzaprice1.destroy()
                                chickenloadedback.destroy()
                                nonVeg()

                            chickenloadedback = Button(frame,
                                                        text="Back",
                                                        command=chickenLoadedFunc,
                                                        image=vpizzabackImg,
                                                        compound=LEFT,
                                                        bg="#ffffff",
                                                        fg="#000000",
                                                        font=vproboto,
                                                        relief=RAISED,
                                                        padx=5,
                                                        pady=5,
                                                        width=160
                                                        )
                            chickenloadedback.place(x=950,y=145)
                    

                    npizza2 = Button(nonvegFrame,
                                     text="Chicken Loaded",
                                     width=285,
                                     height=185,
                                     image=npizza2Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Chicken Loaded": ChickenLoaded(d)
                                     )
                    npizza2.place(x=340,y=10)

#--------------------------------------------------------Chicken Loaded Pizza ends here

                    def Pepperoni(name):
                            insertPizza(name)
                            nonvegFrame.destroy()
                            toppingsFunc()
                            nonvegLogo()
                            

                            ntopContainer1 = Button(root,
                                                    relief=FLAT,
                                                    image=mnpizza3Img,
                                                    width=1285,
                                                    height=260,
                                                    bd=0)
                            ntopContainer1.place(x=0,y=0)
                        
                            
                            npizza1name = Label(detail,
                                                text="Pepperoni",
                                                font=PizzaTitle,
                                                bg="#555353",
                                                fg="#ffffff"
                                                )
                            npizza1name.place(x=70,y=5)

                            npizza1detail = Label(detail,
                                                  text="American Style pizza with Pepperoni and american sauce",
                                                  font=pizzaDes,
                                                  bg="#555353",
                                                  fg="#ffffff",
                                                  wraplength=450,
                                                  justify=LEFT
                                                  )
                            npizza1detail.place(x=25,y=55)

                            npizzaprice1 = Label(detail,
                                                text="Rs 800/-",
                                                bg="#555353",
                                                fg="#ffffff",
                                                font=vproboto,
                                                justify=LEFT,
                                                )
                            npizzaprice1.place(x=25,y=115)

                            sizeL = Button(size,
                                           text="Large 1400/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=1400: insertSize(s))
                            sizeL.place(x=2,y=40)

                            sizeM = Button(size,
                                           text="Medium 800/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=800: insertSize(s))
                            sizeM.place(x=152,y=40)

                            sizeS = Button(size,
                                           text="Small 660/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=660: insertSize(s))
                            sizeS.place(x=302,y=40)

                            qtyFunc()

                            def pepperoniFunc():
                                ntopContainer1.destroy()
                                npizza1name.destroy()
                                npizza1detail.destroy()
                                npizzaprice1.destroy()
                                pepperoniback.destroy()
                                nonVeg()

                            pepperoniback = Button(frame,
                                                        text="Back",
                                                        command=pepperoniFunc,
                                                        image=vpizzabackImg,
                                                        compound=LEFT,
                                                        bg="#ffffff",
                                                        fg="#000000",
                                                        font=vproboto,
                                                        relief=RAISED,
                                                        padx=5,
                                                        pady=5,
                                                        width=160
                                                        )
                            pepperoniback.place(x=950,y=145)

                    npizza3 = Button(nonvegFrame,
                                     text="Pepperoni",
                                     width=285,
                                     height=185,
                                     image=npizza3Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Pepperoni": Pepperoni(d)
                                     )
                    npizza3.place(x=650,y=10)

#--------------------------------------------------------Pepperoni Pizza ends here

                    def ChickenMushroom(name):
                            insertPizza(name)
                            nonvegFrame.destroy()
                            toppingsFunc()
                            nonvegLogo()
                            

                            ntopContainer1 = Button(root,
                                                    relief=FLAT,
                                                    image=mnpizza4Img,
                                                    width=1285,
                                                    height=260,
                                                    bd=0)
                            ntopContainer1.place(x=0,y=0)
                        
                            
                            npizza1name = Label(detail,
                                                text="Chicken Mushroom",
                                                font=PizzaTitle,
                                                bg="#555353",
                                                fg="#ffffff"
                                                )
                            npizza1name.place(x=70,y=5)

                            npizza1detail = Label(detail,
                                                  text="Mixed with chicken & mushroom with spicy sauce and and soft crust",
                                                  font=pizzaDes,
                                                  bg="#555353",
                                                  fg="#ffffff",
                                                  wraplength=450,
                                                  justify=LEFT
                                                  )
                            npizza1detail.place(x=25,y=55)

                            npizzaprice1 = Label(detail,
                                                text="Rs 650/-",
                                                bg="#555353",
                                                fg="#ffffff",
                                                font=vproboto,
                                                justify=LEFT,
                                                )
                            npizzaprice1.place(x=25,y=115)

                            sizeL = Button(size,
                                           text="Large 1000/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=1000: insertSize(s))
                            sizeL.place(x=2,y=40)

                            sizeM = Button(size,
                                           text="Medium 650/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=650: insertSize(s))
                            sizeM.place(x=152,y=40)

                            sizeS = Button(size,
                                           text="Small 500/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=500: insertSize(s))
                            sizeS.place(x=302,y=40)

                            qtyFunc()

                            def chickenmushroomFunc():
                                ntopContainer1.destroy()
                                npizza1name.destroy()
                                npizza1detail.destroy()
                                npizzaprice1.destroy()
                                chickenmushroomback.destroy()
                                nonVeg()

                            chickenmushroomback = Button(frame,
                                                        text="Back",
                                                        command=chickenmushroomFunc,
                                                        image=vpizzabackImg,
                                                        compound=LEFT,
                                                        bg="#ffffff",
                                                        fg="#000000",
                                                        font=vproboto,
                                                        relief=RAISED,
                                                        padx=5,
                                                        pady=5,
                                                        width=160
                                                        )
                            chickenmushroomback.place(x=950,y=145)

                    npizza4 = Button(nonvegFrame,
                                     text="Chicken Mushroom",
                                     width=285,
                                     height=185,
                                     image=npizza4Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Chicken Mushroom": ChickenMushroom(d)
                                     )
                    npizza4.place(x=960,y=10)

#--------------------------------------------------------Chicken Mushroom Pizza ends here

                    def ChickenKeema(name):
                            insertPizza(name)
                            nonvegFrame.destroy()
                            toppingsFunc()
                            nonvegLogo()
                            

                            ntopContainer1 = Button(root,
                                                    relief=FLAT,
                                                    image=mnpizza5Img,
                                                    width=1285,
                                                    height=260,
                                                    bd=0)
                            ntopContainer1.place(x=0,y=0)
                        
                            
                            npizza1name = Label(detail,
                                                text="Chicken Keema",
                                                font=PizzaTitle,
                                                bg="#555353",
                                                fg="#ffffff"
                                                )
                            npizza1name.place(x=70,y=5)

                            npizza1detail = Label(detail,
                                                  text="Chicken keema with Jalapeno with keema sauce and stuffed keema crust",
                                                  font=pizzaDes,
                                                  bg="#555353",
                                                  fg="#ffffff",
                                                  wraplength=450,
                                                  justify=LEFT
                                                  )
                            npizza1detail.place(x=25,y=55)

                            npizzaprice1 = Label(detail,
                                                text="Rs 850/-",
                                                bg="#555353",
                                                fg="#ffffff",
                                                font=vproboto,
                                                justify=LEFT,
                                                )
                            npizzaprice1.place(x=25,y=115)

                            sizeL = Button(size,
                                           text="Large 1500/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=1500: insertSize(s))
                            sizeL.place(x=2,y=40)

                            sizeM = Button(size,
                                           text="Medium 850/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=850: insertSize(s))
                            sizeM.place(x=152,y=40)

                            sizeS = Button(size,
                                           text="Small 590/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=590: insertSize(s))
                            sizeS.place(x=302,y=40)

                            qtyFunc()

                            def chickenkeemaFunc():
                                ntopContainer1.destroy()
                                npizza1name.destroy()
                                npizza1detail.destroy()
                                npizzaprice1.destroy()
                                chickenkeemaback.destroy()
                                nonVeg()

                            chickenkeemaback = Button(frame,
                                                        text="Back",
                                                        command=chickenkeemaFunc,
                                                        image=vpizzabackImg,
                                                        compound=LEFT,
                                                        bg="#ffffff",
                                                        fg="#000000",
                                                        font=vproboto,
                                                        relief=RAISED,
                                                        padx=5,
                                                        pady=5,
                                                        width=160
                                                        )
                            chickenkeemaback.place(x=950,y=145)

                    npizza5 = Button(nonvegFrame,
                                     text="Chicken Keema",
                                     width=285,
                                     height=185,
                                     image=npizza5Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Chicken Keema": ChickenKeema(d)
                                     )
                    npizza5.place(x=30,y=220)

#--------------------------------------------------------Chicken Keema Pizza ends here

                    def MixedChicken(name):
                            insertPizza(name)
                            nonvegFrame.destroy()
                            toppingsFunc()
                            nonvegLogo()
                            

                            ntopContainer1 = Button(root,
                                                    relief=FLAT,
                                                    image=mnpizza6Img,
                                                    width=1285,
                                                    height=260,
                                                    bd=0)
                            ntopContainer1.place(x=0,y=0)
                        
                            
                            npizza1name = Label(detail,
                                                text="Mixed Chicken",
                                                font=PizzaTitle,
                                                bg="#555353",
                                                fg="#ffffff"
                                                )
                            npizza1name.place(x=70,y=5)

                            npizza1detail = Label(detail,
                                                  text="Chicken with diffrent type and tikka sauce with stuffed chicken crust",
                                                  font=pizzaDes,
                                                  bg="#555353",
                                                  fg="#ffffff",
                                                  wraplength=450,
                                                  justify=LEFT
                                                  )
                            npizza1detail.place(x=25,y=55)

                            npizzaprice1 = Label(detail,
                                                text="Rs 770/-",
                                                bg="#555353",
                                                fg="#ffffff",
                                                font=vproboto,
                                                justify=LEFT,
                                                )
                            npizzaprice1.place(x=25,y=115)

                            sizeL = Button(size,
                                           text="Large 900/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=900: insertSize(s))
                            sizeL.place(x=2,y=40)

                            sizeM = Button(size,
                                           text="Medium 770/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=770: insertSize(s))
                            sizeM.place(x=152,y=40)

                            sizeS = Button(size,
                                           text="Small 570/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=570: insertSize(s))
                            sizeS.place(x=302,y=40)

                            qtyFunc()

                            def mixedchickenFunc():
                                ntopContainer1.destroy()
                                npizza1name.destroy()
                                npizza1detail.destroy()
                                npizzaprice1.destroy()
                                mixedchickenback.destroy()
                                nonVeg()

                            mixedchickenback = Button(frame,
                                                        text="Back",
                                                        command=mixedchickenFunc,
                                                        image=vpizzabackImg,
                                                        compound=LEFT,
                                                        bg="#ffffff",
                                                        fg="#000000",
                                                        font=vproboto,
                                                        relief=RAISED,
                                                        padx=5,
                                                        pady=5,
                                                        width=160
                                                        )
                            mixedchickenback.place(x=950,y=145)

                    npizza6 = Button(nonvegFrame,
                                     text="Mixed Chicken",
                                     width=285,
                                     height=185,
                                     image=npizza6Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Mixed Chicken": MixedChicken(d)
                                     )
                    npizza6.place(x=340,y=220)

#--------------------------------------------------------Mixed Chicken Pizza ends here

                    def PepperChicken(name):
                            insertPizza(name)
                            nonvegFrame.destroy()
                            toppingsFunc()
                            nonvegLogo()
                            

                            ntopContainer1 = Button(root,
                                                    relief=FLAT,
                                                    image=mnpizza7Img,
                                                    width=1285,
                                                    height=260,
                                                    bd=0)
                            ntopContainer1.place(x=0,y=0)
                        
                            
                            npizza1name = Label(detail,
                                                text="Pepper Chicken",
                                                font=PizzaTitle,
                                                bg="#555353",
                                                fg="#ffffff"
                                                )
                            npizza1name.place(x=70,y=5)

                            npizza1detail = Label(detail,
                                                  text="Chicken with pepper and spicy sauce and wheat crust",
                                                  font=pizzaDes,
                                                  bg="#555353",
                                                  fg="#ffffff",
                                                  wraplength=450,
                                                  justify=LEFT
                                                  )
                            npizza1detail.place(x=25,y=55)

                            npizzaprice1 = Label(detail,
                                                text="Rs 630/-",
                                                bg="#555353",
                                                fg="#ffffff",
                                                font=vproboto,
                                                justify=LEFT,
                                                )
                            npizzaprice1.place(x=25,y=115)

                            sizeL = Button(size,
                                           text="Large 880/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=880: insertSize(s))
                            sizeL.place(x=2,y=40)

                            sizeM = Button(size,
                                           text="Medium 630/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=630: insertSize(s))
                            sizeM.place(x=152,y=40)

                            sizeS = Button(size,
                                           text="Small 400/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=400: insertSize(s))
                            sizeS.place(x=302,y=40)

                            qtyFunc()

                            def pepperchickenFunc():
                                ntopContainer1.destroy()
                                npizza1name.destroy()
                                npizza1detail.destroy()
                                npizzaprice1.destroy()
                                pepperchickenback.destroy()
                                nonVeg()

                            pepperchickenback = Button(frame,
                                                        text="Back",
                                                        command=pepperchickenFunc,
                                                        image=vpizzabackImg,
                                                        compound=LEFT,
                                                        bg="#ffffff",
                                                        fg="#000000",
                                                        font=vproboto,
                                                        relief=RAISED,
                                                        padx=5,
                                                        pady=5,
                                                        width=160
                                                        )
                            pepperchickenback.place(x=950,y=145)

                    npizza7 = Button(nonvegFrame,
                                     text="Pepper Chicken",
                                     width=285,
                                     height=185,
                                     image=npizza7Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Pepper Chicken": PepperChicken(d)
                                     )
                    npizza7.place(x=650,y=220)

#--------------------------------------------------------Pepper Chicken Pizza ends here

                    def SupremeChicken(name):
                            insertPizza(name)
                            nonvegFrame.destroy()
                            toppingsFunc()
                            nonvegLogo()
                            

                            ntopContainer1 = Button(root,
                                                    relief=FLAT,
                                                    image=mnpizza8Img,
                                                    width=1285,
                                                    height=260,
                                                    bd=0)
                            ntopContainer1.place(x=0,y=0)
                        
                            
                            npizza1name = Label(detail,
                                                text="Supreme Chicken",
                                                font=PizzaTitle,
                                                bg="#555353",
                                                fg="#ffffff"
                                                )
                            npizza1name.place(x=70,y=5)

                            npizza1detail = Label(detail,
                                                  text="Supreme all chicken pizza loaded with chicken and vegi with special italian sauce with soft crust",
                                                  font=pizzaDes,
                                                  bg="#555353",
                                                  fg="#ffffff",
                                                  wraplength=450,
                                                  justify=LEFT
                                                  )
                            npizza1detail.place(x=25,y=55)

                            npizzaprice1 = Label(detail,
                                                text="Rs 800/-",
                                                bg="#555353",
                                                fg="#ffffff",
                                                font=vproboto,
                                                justify=LEFT,
                                                )
                            npizzaprice1.place(x=25,y=115)

                            sizeL = Button(size,
                                           text="Large 1200/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=1200: insertSize(s))
                            sizeL.place(x=2,y=40)

                            sizeM = Button(size,
                                           text="Medium 800/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=800: insertSize(s))
                            sizeM.place(x=152,y=40)

                            sizeS = Button(size,
                                           text="Small 650/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=650: insertSize(s))
                            sizeS.place(x=302,y=40)

                            qtyFunc()

                            def supremechickenFunc():
                                ntopContainer1.destroy()
                                npizza1name.destroy()
                                npizza1detail.destroy()
                                npizzaprice1.destroy()
                                supremechickenback.destroy()
                                nonVeg()

                            supremechickenback = Button(frame,
                                                        text="Back",
                                                        command=supremechickenFunc,
                                                        image=vpizzabackImg,
                                                        compound=LEFT,
                                                        bg="#ffffff",
                                                        fg="#000000",
                                                        font=vproboto,
                                                        relief=RAISED,
                                                        padx=5,
                                                        pady=5,
                                                        width=160
                                                        )
                            supremechickenback.place(x=950,y=145)

                    npizza8 = Button(nonvegFrame,
                                     text="Supreme Chicken",
                                     width=285,
                                     height=185,
                                     image=npizza8Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Supreme Chicken": SupremeChicken(d)
                                     )
                    npizza8.place(x=960,y=220)

#--------------------------------------------------------Supreme Chicken Pizza ends here

                    def SweetChicken(name):
                            insertPizza(name)
                            nonvegFrame.destroy()
                            toppingsFunc()
                            nonvegLogo()
                            

                            ntopContainer1 = Button(root,
                                                    relief=FLAT,
                                                    image=mnpizza9Img,
                                                    width=1285,
                                                    height=260,
                                                    bd=0)
                            ntopContainer1.place(x=0,y=0)
                        
                            
                            npizza1name = Label(detail,
                                                text="Sweet Chicken",
                                                font=PizzaTitle,
                                                bg="#555353",
                                                fg="#ffffff"
                                                )
                            npizza1name.place(x=70,y=5)

                            npizza1detail = Label(detail,
                                                  text="Loaded with chicken and pineapple with italian sauce and soft crust",
                                                  font=pizzaDes,
                                                  bg="#555353",
                                                  fg="#ffffff",
                                                  wraplength=450,
                                                  justify=LEFT
                                                  )
                            npizza1detail.place(x=25,y=55)

                            npizzaprice1 = Label(detail,
                                                text="Rs 530/-",
                                                bg="#555353",
                                                fg="#ffffff",
                                                font=vproboto,
                                                justify=LEFT,
                                                )
                            npizzaprice1.place(x=25,y=115)

                            sizeL = Button(size,
                                           text="Large 780/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=780: insertSize(s))
                            sizeL.place(x=2,y=40)

                            sizeM = Button(size,
                                           text="Medium 550/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=550: insertSize(s))
                            sizeM.place(x=152,y=40)

                            sizeS = Button(size,
                                           text="Small 320/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=320: insertSize(s))
                            sizeS.place(x=302,y=40)

                            qtyFunc()

                            def sweetchickenFunc():
                                ntopContainer1.destroy()
                                npizza1name.destroy()
                                npizza1detail.destroy()
                                npizzaprice1.destroy()
                                sweetchickenback.destroy()
                                nonVeg()

                            sweetchickenback = Button(frame,
                                                        text="Back",
                                                        command=sweetchickenFunc,
                                                        image=vpizzabackImg,
                                                        compound=LEFT,
                                                        bg="#ffffff",
                                                        fg="#000000",
                                                        font=vproboto,
                                                        relief=RAISED,
                                                        padx=5,
                                                        pady=5,
                                                        width=160
                                                        )
                            sweetchickenback.place(x=950,y=145)

                    npizza9 = Button(nonvegFrame,
                                     text="Sweet Chicken",
                                     width=285,
                                     height=185,
                                     image=npizza9Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                     command=lambda d="Sweet Chicken":SweetChicken(d)
                                     )
                    npizza9.place(x=170,y=430)

#--------------------------------------------------------Sweet Chicken Pizza ends here

                    def TheSpicy(name):
                            insertPizza(name)
                            nonvegFrame.destroy()
                            toppingsFunc()
                            nonvegLogo()
                            

                            ntopContainer1 = Button(root,
                                                    relief=FLAT,
                                                    image=mnpizza10Img,
                                                    width=1285,
                                                    height=260,
                                                    bd=0)
                            ntopContainer1.place(x=0,y=0)
                        
                            
                            npizza1name = Label(detail,
                                                text="The Spicy",
                                                font=PizzaTitle,
                                                bg="#555353",
                                                fg="#ffffff"
                                                )
                            npizza1name.place(x=70,y=5)

                            npizza1detail = Label(detail,
                                                  text="Spicy Chicken with Indian spicy sauce and wheat crust",
                                                  font=pizzaDes,
                                                  bg="#555353",
                                                  fg="#ffffff",
                                                  wraplength=450,
                                                  justify=LEFT
                                                  )
                            npizza1detail.place(x=25,y=55)

                            npizzaprice1 = Label(detail,
                                                text="Rs 700/-",
                                                bg="#555353",
                                                fg="#ffffff",
                                                font=vproboto,
                                                justify=LEFT,
                                                )
                            npizzaprice1.place(x=25,y=115)

                            sizeL = Button(size,
                                           text="Large 999/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=999: insertSize(s))
                            sizeL.place(x=2,y=40)

                            sizeM = Button(size,
                                           text="Medium 700/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=700: insertSize(s))
                            sizeM.place(x=152,y=40)

                            sizeS = Button(size,
                                           text="Small 450/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=450: insertSize(s))
                            sizeS.place(x=302,y=40)

                            qtyFunc()

                            def thespicyFunc():
                                ntopContainer1.destroy()
                                npizza1name.destroy()
                                npizza1detail.destroy()
                                npizzaprice1.destroy()
                                thespicyback.destroy()
                                nonVeg()

                            thespicyback = Button(frame,
                                                        text="Back",
                                                        command=thespicyFunc,
                                                        image=vpizzabackImg,
                                                        compound=LEFT,
                                                        bg="#ffffff",
                                                        fg="#000000",
                                                        font=vproboto,
                                                        relief=RAISED,
                                                        padx=5,
                                                        pady=5,
                                                        width=160
                                                        )
                            thespicyback.place(x=950,y=145)

                    npizza10 = Button(nonvegFrame,
                                     text="The Spicy",
                                     width=285,
                                     height=185,
                                     image=npizza10Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                      command=lambda d="The Spicy": TheSpicy(d)
                                     )
                    npizza10.place(x=480,y=430)

#--------------------------------------------------------Thw Spicy Pizza ends here

                    def ChickenSausage(name):
                            insertPizza(name)
                            nonvegFrame.destroy()
                            toppingsFunc()
                            nonvegLogo()
                            

                            ntopContainer1 = Button(root,
                                                    relief=FLAT,
                                                    image=mnpizza11Img,
                                                    width=1285,
                                                    height=260,
                                                    bd=0)
                            ntopContainer1.place(x=0,y=0)
                        
                            
                            npizza1name = Label(detail,
                                                text="Chicken Sausage",
                                                font=PizzaTitle,
                                                bg="#555353",
                                                fg="#ffffff"
                                                )
                            npizza1name.place(x=70,y=5)

                            npizza1detail = Label(detail,
                                                  text="Chicken Sausage with white sauce and crunchy crust",
                                                  font=pizzaDes,
                                                  bg="#555353",
                                                  fg="#ffffff",
                                                  wraplength=450,
                                                  justify=LEFT
                                                  )
                            npizza1detail.place(x=25,y=55)

                            npizzaprice1 = Label(detail,
                                                text="Rs 590/-",
                                                bg="#555353",
                                                fg="#ffffff",
                                                font=vproboto,
                                                justify=LEFT,
                                                )
                            npizzaprice1.place(x=25,y=115)

                            sizeL = Button(size,
                                           text="Large 780/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=780: insertSize(s))
                            sizeL.place(x=2,y=40)

                            sizeM = Button(size,
                                           text="Medium 590/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=590: insertSize(s))
                            sizeM.place(x=152,y=40)

                            sizeS = Button(size,
                                           text="Small 300/-",
                                           bg="#000000",
                                           fg="#ffffff",
                                           image=sizeImg,
                                           font=pizzaDes,
                                           compound=TOP,
                                           height=115,
                                           width=120,
                                           relief=FLAT,
                                           pady=8,
                                           command=lambda s=300: insertSize(s))
                            sizeS.place(x=302,y=40)

                            qtyFunc()

                            def chickensausageFunc():
                                ntopContainer1.destroy()
                                npizza1name.destroy()
                                npizza1detail.destroy()
                                npizzaprice1.destroy()
                                chickensausageback.destroy()
                                size.destroy()
                                nonVeg()

                            chickensausageback = Button(frame,
                                                        text="Back",
                                                        command=chickensausageFunc,
                                                        image=vpizzabackImg,
                                                        compound=LEFT,
                                                        bg="#ffffff",
                                                        fg="#000000",
                                                        font=vproboto,
                                                        relief=RAISED,
                                                        padx=5,
                                                        pady=5,
                                                        width=160
                                                        )
                            chickensausageback.place(x=950,y=145)

                    npizza11 = Button(nonvegFrame,
                                     text="Chicken Sausage",
                                     width=285,
                                     height=185,
                                     image=npizza11Img,
                                     compound=TOP,
                                     bd=0,
                                     bg="#000000",
                                     fg="#ffffff",
                                     relief=FLAT,
                                     font=vproboto,
                                      command=lambda d="Chicken Sausage": ChickenSausage(d)
                                     )
                    npizza11.place(x=790,y=430)

                    def nonvegBack():
                        nonvegFrame.destroy()
                        menu()

                    npizzaback = Button(nonvegFrame,
                                        text="Back",
                                        fg="#000000",
                                        bg="#ffffff",
                                        font=vproboto,
                                        image=vpizzabackImg,
                                        compound=LEFT,
                                        padx=10,
                                        command=nonvegBack
                                        )
                    npizzaback.place(x=20,y=570)


#Non-veg pizzas ends here-------------------------------------------------------------------------------------


#-----------------------------------------------------------------Desserts Function starts-----------------------------------------------------------------

                def dessert():
                    vegPizzaBtn.destroy()
                    nonPizzaBtn.destroy()
                    dessertBtn.destroy()
                    viewCart.destroy()
                    logout.destroy()

                    dessertframe = Frame(root,height=660,width=1280,bg="#555353")
                    dessertframe.place(x=0,y=0)

                    packs = Frame(dessertframe,height=300, width=700, bg="#555353")
                    packs.place(x=520,y=300)

                    
                    dFlavour = Frame(dessertframe,height=300, width=755, bg="#555353")
                    dFlavour.place(x=520,y=2)

                    def qtyFunc1():
                        def value1():
                            cv=qty_sb.get()
                            dbqty = MySQLdb.connect("localhost","root","","pizza")
                            c_qty = dbqty.cursor()
                            try:
                                q_qty = "update pizzadetail set qty=%s where pno=%s"
                                qty_data=(cv,j)
                                c_qty.execute(q_qty,qty_data)
                                dbqty.commit()
                            except Exception as ex:
                                dbqty.rollback()
                                messagebox.showerror("Pizza Station","Error!!")
                                print(ex)
                            dbqty.close()

                        qty_name = Label(packs,text="Quantity",bg="#555353",fg="#ffffff",font=toppings)
                        qty_name.place(x=490,y=90)

                        qty_sb = Spinbox(packs, from_=0, to = 5,bg="#ffffff",fg="#000000",command=value1,font=Font(family='Helvetica', size=28, weight='bold'),width=5)
                        qty_sb.place(x=490,y=130)

                    def flavours():
                        fName = Label(dFlavour,text="Flavours",bg="#555353",fg="#ffffff",font=PizzaTitle)
                        fName.place(x=5,y=5)

                        f1 = Button(dFlavour,
                                    text="Blueberry",
                                    bg="#000000",
                                    fg="#ffffff",
                                    font=pizzaDes,
                                    compound=TOP,
                                    image=flavour1Img,
                                    padx=5,
                                    pady=5)
                        f1.place(x=5,y=55)


                        f2 = Button(dFlavour,
                                    text="Chocolate",
                                    bg="#000000",
                                    fg="#ffffff",
                                    font=pizzaDes,
                                    compound=TOP,
                                    image=flavour2Img,
                                    padx=5,
                                    pady=5)
                        f2.place(x=200,y=55)


                        f3 = Button(dFlavour,
                                    text="Honey",
                                    bg="#000000",
                                    fg="#ffffff",
                                    font=pizzaDes,
                                    compound=TOP,
                                    image=flavour3Img,
                                    padx=5,
                                    pady=5)
                        f3.place(x=395,y=55)

                        f4 = Button(dFlavour,
                                    text="Coffee",
                                    bg="#000000",
                                    fg="#ffffff",
                                    font=pizzaDes,
                                    compound=TOP,
                                    image=flavour4Img,
                                    padx=5,
                                    pady=5)
                        f4.place(x=590,y=55)

                        def changeD():
                            Sbutton.config(bg="#21E171",image=done,text="Done",padx=5,pady=5,width=160)

                        Sbutton = Button(root,
                                         text="Add To cart",
                                         image=cart,
                                         compound=LEFT,
                                         relief=RAISED,
                                         font=vproboto,
                                         fg="#ffffff",
                                         bg="#6C9BEA",
                                         padx=5,
                                         pady=5,
                                         command=changeD
                                         )
                        Sbutton.place(x=530,y=580)


                    def brownieFunc(name):
                        insertPizza(name)
                        dessert1.destroy()
                        dessert2.destroy()
                        dessert3.destroy()
                        dessert4.destroy()
                        dessert5.destroy()
                        dessert6.destroy()
                        dessert7.destroy()
                        flavours()
                        
                        dessert_container = Label(dessertframe,image=mdessert1,bg="#555353",bd=0)
                        dessert_container.place(x=0,y=0)

                        pName = Label(packs,text="Packs",bg="#555353",fg="#ffffff",font=PizzaTitle)
                        pName.place(x=5,y=5)

                        B2P = Button(packs,
                                     text="Rs.180/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack2,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=180: insertSize(s)
                                     )
                        B2P.place(x=5,y=60)

                        B4P = Button(packs,
                                     text="Rs.300/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack4,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=300: insertSize(s)
                                     )
                        B4P.place(x=155,y=60)

                        B6P = Button(packs,
                                     text="Rs.400/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack6,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=400: insertSize(s)
                                     )
                        B6P.place(x=305,y=60)

                        qtyFunc1()

                        def dBackFunc():
                            dessert_container.destroy()
                            packs.destroy()
                            dFlavour.destroy()
                            Bbutton.destroy()
                            dessert()
                            

                        Bbutton = Button(root,
                                         text="Back",
                                         command=dBackFunc,
                                         image=vpizzabackImg,
                                         compound=LEFT,
                                         bg="#ffffff",
                                         fg="#000000",
                                         font=vproboto,
                                         relief=RAISED,
                                         padx=5,
                                         pady=5,
                                         width=160
                                         )
                        Bbutton.place(x=1050,y=580)

                    dessert1 = Button(dessertframe,
                                      text="Brownie",
                                      relief=FLAT,
                                      bg="#000000",
                                      fg="#ffffff",
                                      image=dessertImg1,
                                      compound=TOP,
                                      font=roboto,
                                      width=200,
                                      height=280,
                                      padx=2,
                                      pady=8,
                                      command=lambda d="Brownie":brownieFunc(d)
                                      )
                                      
                    dessert1.place(x=90,y=20)

                    def cupcakeFunc(name):
                        insertPizza(name)
                        dessert1.destroy()
                        dessert2.destroy()
                        dessert3.destroy()
                        dessert4.destroy()
                        dessert5.destroy()
                        dessert6.destroy()
                        dessert7.destroy()
                        flavours()
                        
                        dessert_container = Label(dessertframe,image=mdessert2,bg="#555353",bd=0)
                        dessert_container.place(x=0,y=0)

                        pName = Label(packs,text="Packs",bg="#555353",fg="#ffffff",font=PizzaTitle)
                        pName.place(x=5,y=5)

                        B2P = Button(packs,
                                     text="Rs.150/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack4,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=150: insertSize(s)
                                     )
                        B2P.place(x=5,y=60)

                        B4P = Button(packs,
                                     text="Rs.220/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack6,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=220: insertSize(s)
                                     )
                        B4P.place(x=155,y=60)

                        B6P = Button(packs,
                                     text="Rs.300/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack8,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=300: insertSize(s)
                                     )
                        B6P.place(x=305,y=60)

                        qtyFunc1()

                        def dBackFunc():
                            dessert_container.destroy()
                            packs.destroy()
                            dFlavour.destroy()
                            Bbutton.destroy()
                            dessert()
                            

                        Bbutton = Button(root,
                                         text="Back",
                                         command=dBackFunc,
                                         image=vpizzabackImg,
                                         compound=LEFT,
                                         bg="#ffffff",
                                         fg="#000000",
                                         font=vproboto,
                                         relief=RAISED,
                                         padx=5,
                                         pady=5,
                                         width=160
                                         )
                        Bbutton.place(x=1050,y=580)

                    

                    dessert2 = Button(dessertframe,
                                      text="Cupcakes",
                                      relief=FLAT,
                                      bg="#000000",
                                      fg="#ffffff",
                                      image=dessertImg2,
                                      compound=TOP,
                                      font=roboto,
                                      width=200,
                                      height=280,
                                      padx=2,
                                      pady=8,
                                      command=lambda d="Cupcakes": cupcakeFunc(d)
                                      )
                                      
                    dessert2.place(x=380,y=20)

                    def donutFunc(name):
                        insertPizza(name)
                        dessert1.destroy()
                        dessert2.destroy()
                        dessert3.destroy()
                        dessert4.destroy()
                        dessert5.destroy()
                        dessert6.destroy()
                        dessert7.destroy()
                        flavours()
                        
                        dessert_container = Label(dessertframe,image=mdessert3,bg="#555353",bd=0)
                        dessert_container.place(x=0,y=0)

                        pName = Label(packs,text="Packs",bg="#555353",fg="#ffffff",font=PizzaTitle)
                        pName.place(x=5,y=5)

                        B2P = Button(packs,
                                     text="Rs.160/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack2,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=160: insertSize(s)
                                     )
                        B2P.place(x=5,y=60)

                        B4P = Button(packs,
                                     text="Rs.240/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack3,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=240: insertSize(s)
                                     )
                        B4P.place(x=155,y=60)

                        B6P = Button(packs,
                                     text="Rs.320/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack4,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=320: insertSize(s)
                                     )
                        B6P.place(x=305,y=60)

                        qtyFunc1()

                        def dBackFunc():
                            dessert_container.destroy()
                            packs.destroy()
                            dFlavour.destroy()
                            Bbutton.destroy()
                            dessert()
                            

                        Bbutton = Button(root,
                                         text="Back",
                                         command=dBackFunc,
                                         image=vpizzabackImg,
                                         compound=LEFT,
                                         bg="#ffffff",
                                         fg="#000000",
                                         font=vproboto,
                                         relief=RAISED,
                                         padx=5,
                                         pady=5,
                                         width=160
                                         )
                        Bbutton.place(x=1050,y=580)

                    dessert3 = Button(dessertframe,
                                      text="Donut",
                                      relief=FLAT,
                                      bg="#000000",
                                      fg="#ffffff",
                                      image=dessertImg3,
                                      compound=TOP,
                                      font=roboto,
                                      width=200,
                                      height=280,
                                      padx=2,
                                      pady=8,
                                      command=lambda d="Donut": donutFunc(d)
                                      )
                                      
                    dessert3.place(x=670,y=20)


                    def pastryFunc(name):
                        insertPizza(name)
                        dessert1.destroy()
                        dessert2.destroy()
                        dessert3.destroy()
                        dessert4.destroy()
                        dessert5.destroy()
                        dessert6.destroy()
                        dessert7.destroy()
                        flavours()
                        
                        dessert_container = Label(dessertframe,image=mdessert4,bg="#555353",bd=0)
                        dessert_container.place(x=0,y=0)

                        pName = Label(packs,text="Packs",bg="#555353",fg="#ffffff",font=PizzaTitle)
                        pName.place(x=5,y=5)

                        B2P = Button(packs,
                                     text="Rs.90/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack1,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=90: insertSize(s)
                                     )
                        B2P.place(x=5,y=60)

                        B4P = Button(packs,
                                     text="Rs.170/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack2,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=170: insertSize(s)
                                     )
                        B4P.place(x=155,y=60)

                        B6P = Button(packs,
                                     text="Rs.250/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack3,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=250: insertSize(s)
                                     )
                        B6P.place(x=305,y=60)

                        qtyFunc1()

                        def dBackFunc():
                            dessert_container.destroy()
                            packs.destroy()
                            dFlavour.destroy()
                            Bbutton.destroy()
                            dessert()
                            

                        Bbutton = Button(root,
                                         text="Back",
                                         command=dBackFunc,
                                         image=vpizzabackImg,
                                         compound=LEFT,
                                         bg="#ffffff",
                                         fg="#000000",
                                         font=vproboto,
                                         relief=RAISED,
                                         padx=5,
                                         pady=5,
                                         width=160
                                         )
                        Bbutton.place(x=1050,y=580)

                    dessert4 = Button(dessertframe,
                                      text="Pastry",
                                      relief=FLAT,
                                      bg="#000000",
                                      fg="#ffffff",
                                      image=dessertImg4,
                                      compound=TOP,
                                      font=roboto,
                                      width=200,
                                      height=280,
                                      padx=2,
                                      pady=8,
                                      command=lambda d="Pastry": pastryFunc(d)
                                      )
                                      
                    dessert4.place(x=960,y=20)

                    def pancakesFunc(name):
                        insertPizza(name)
                        dessert1.destroy()
                        dessert2.destroy()
                        dessert3.destroy()
                        dessert4.destroy()
                        dessert5.destroy()
                        dessert6.destroy()
                        dessert7.destroy()
                        flavours()
                        
                        dessert_container = Label(dessertframe,image=mdessert5,bg="#555353",bd=0)
                        dessert_container.place(x=0,y=0)

                        pName = Label(packs,text="Packs",bg="#555353",fg="#ffffff",font=PizzaTitle)
                        pName.place(x=5,y=5)

                        B2P = Button(packs,
                                     text="Rs.150/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack4,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=150: insertSize(s)
                                     )
                        B2P.place(x=5,y=60)

                        B4P = Button(packs,
                                     text="Rs.250/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack6,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=250: insertSize(s)
                                     )
                        B4P.place(x=155,y=60)

                        B6P = Button(packs,
                                     text="Rs.350/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack8,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=350: insertSize(s)
                                     )
                        B6P.place(x=305,y=60)

                        qtyFunc1()

                        def dBackFunc():
                            dessert_container.destroy()
                            packs.destroy()
                            dFlavour.destroy()
                            Bbutton.destroy()
                            dessert()
                            

                        Bbutton = Button(root,
                                         text="Back",
                                         command=dBackFunc,
                                         image=vpizzabackImg,
                                         compound=LEFT,
                                         bg="#ffffff",
                                         fg="#000000",
                                         font=vproboto,
                                         relief=RAISED,
                                         padx=5,
                                         pady=5,
                                         width=160
                                         )
                        Bbutton.place(x=1050,y=580)

                    dessert5 = Button(dessertframe,
                                      text="Pancakes",
                                      relief=FLAT,
                                      bg="#000000",
                                      fg="#ffffff",
                                      image=dessertImg5,
                                      compound=TOP,
                                      font=roboto,
                                      width=200,
                                      height=280,
                                      padx=2,
                                      pady=8,
                                      command=lambda d="Pancakes": pancakesFunc(d)
                                      )
                                      
                    dessert5.place(x=230,y=340)

                    def pieFunc():
                        messagebox.showinfo("Pizza Station","Out Of Stock")

                    dessert6 = Button(dessertframe,
                                      text="Pie",
                                      relief=FLAT,
                                      bg="#000000",
                                      fg="#ffffff",
                                      image=dessertImg6,
                                      compound=TOP,
                                      font=roboto,
                                      width=200,
                                      height=280,
                                      padx=2,
                                      pady=8,
                                      command=pieFunc
                                      )
                                      
                    dessert6.place(x=520,y=340)

                    def macaronsFunc(name):
                        insertPizza(name)
                        dessert1.destroy()
                        dessert2.destroy()
                        dessert3.destroy()
                        dessert4.destroy()
                        dessert5.destroy()
                        dessert6.destroy()
                        dessert7.destroy()
                        flavours()
                        
                        dessert_container = Label(dessertframe,image=mdessert7,bg="#555353",bd=0)
                        dessert_container.place(x=0,y=0)

                        pName = Label(packs,text="Packs",bg="#555353",fg="#ffffff",font=PizzaTitle)
                        pName.place(x=5,y=5)

                        B2P = Button(packs,
                                     text="Rs.180/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack6,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=180: insertSize(s)
                                     )
                        B2P.place(x=5,y=60)

                        B4P = Button(packs,
                                     text="Rs.240/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack8,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=240: insertSize(s)
                                     )
                        B4P.place(x=155,y=60)

                        B6P = Button(packs,
                                     text="Rs.360/-",
                                     bg="#000000",
                                     fg="#ffffff",
                                     image=Bpack12,
                                     font=pizzaDes,
                                     compound=TOP,
                                     relief=FLAT,
                                     command=lambda s=360: insertSize(s)
                                     )
                        B6P.place(x=305,y=60)

                        qtyFunc1()

                        def dBackFunc():
                            dessert_container.destroy()
                            packs.destroy()
                            dFlavour.destroy()
                            Bbutton.destroy()
                            dessert()
                            

                        Bbutton = Button(root,
                                         text="Back",
                                         command=dBackFunc,
                                         image=vpizzabackImg,
                                         compound=LEFT,
                                         bg="#ffffff",
                                         fg="#000000",
                                         font=vproboto,
                                         relief=RAISED,
                                         padx=5,
                                         pady=5,
                                         width=160
                                         )
                        Bbutton.place(x=1050,y=580)

                    dessert7 = Button(dessertframe,
                                      text="Macarons",
                                      relief=FLAT,
                                      bg="#000000",
                                      fg="#ffffff",
                                      image=dessertImg7,
                                      compound=TOP,
                                      font=roboto,
                                      width=200,
                                      height=280,
                                      padx=2,
                                      pady=8,
                                      command=lambda d="Macarons": macaronsFunc(d)
                                      )
                                      
                    dessert7.place(x=810,y=340)

                    def dessertBack():
                        dessertframe.destroy()
                        menu()

                    dessertBackBtn= Button(dessertframe,
                                           text="Back",
                                           fg="#000000",
                                           bg="#ffffff",
                                           font=vproboto,
                                           image=vpizzabackImg,
                                           compound=LEFT,
                                           padx=10,
                                           command=dessertBack
                                           )
                    dessertBackBtn.place(x=20,y=570)
                    
#----------------------------------------------------------------------Menu Function starts--------------------------------------------------------------

                vegPizzaBtn = Button(root,
                                     text="Veg Pizza",
                                     relief=FLAT,
                                     image=vegPImg,
                                     compound=TOP,
                                     width=285,
                                     height=485,
                                     bg="#000000",
                                     fg="#ffffff",            #Veg Menu Button-----
                                     font=roboto,
                                     padx=10,
                                     pady=4,
                                     command=vegPizza
                                     )
                vegPizzaBtn.place(x=70,y=50);

                nonPizzaBtn = Button(root,
                                     text="Non-veg Pizza",
                                     relief=FLAT,
                                     image=nonPImg,
                                     compound=TOP,
                                     width=285,                 #Non-Veg Menu Button------
                                     height=485,
                                     bg="#000000",
                                     fg="#ffffff",
                                     font=roboto,
                                     padx=10,
                                     pady=4,
                                     command=nonVeg
                                     )
                nonPizzaBtn.place(x=470,y=50);

                dessertBtn = Button(root,
                                    text="Dessert",
                                    relief=FLAT,
                                    image=dessertImg,
                                    compound=TOP,
                                    width=285,                  #Dessert Menu Button
                                    height=485,
                                    bg="#000000",
                                    fg="#ffffff",
                                    font=roboto,
                                    padx=10,
                                    pady=4,
                                    command=dessert
                                    )
                dessertBtn.place(x=870,y=50);


                def viewCartFunc():
                    vegPizzaBtn.destroy()
                    nonPizzaBtn.destroy()
                    dessertBtn.destroy()
                    x1=0
                    y1=0
                    fx=5
                    fy=5
                    lselection=1
                    nonVegList=["Cheesy Chicken","Chicken Loaded","Pepperoni","Chicken Mushroom","Chicken Keema","Mixed Chicken","Pepper Chicken","Supreme Chicken","Sweet Chicken","The Spicy","Chicken Sausage"]

                    hide=Frame(root,height=660,width=1290,bg="#555353")
                    hide.place(x=0,y=0)


                    alldb = MySQLdb.connect("localhost","root","","pizza")
                    allC=alldb.cursor()

                    try:
                        allQ="select * from pizzadetail"
                        allC.execute(allQ)
                        allData=allC.fetchall()
                        all_price=[]
                        all_toppings=[]
                        all_names=[]
                        all_qty=[]
                        for allD in allData:
                            all_names.append(allD[1]) #All Pizza names
                            all_price.append(allD[2])  #All the Prices of the Pizza
                            all_toppings.append(allD[3]) #All Toppings
                            all_qty.append(allD[4]) #All Pizzas Quantity 
                            #print(allD[3])
                        alldb.commit()
                    except Exception as ex1:
                        print(ex1)
                        alldb.rollback()
                    alldb.close()

                    ol1size=len(all_names)-1


                    cartCanvas = tk.Canvas(root,bg="#555353",height=660,width=600,highlightthickness=0)

                    vsb = tk.Scrollbar(root,orient="vertical",command=cartCanvas.yview)
                    cartFrame = tk.Frame(cartCanvas)
                    
                    
                    for i in range(1,ol1size+1):
                        orderframe = Frame(cartCanvas,height=100,width=500,bg="#ffffff")
                        cartCanvas.create_window(fx,fy, window=orderframe)
                        
                        pizzaName = Label(orderframe,text=all_names[lselection],font=toppings,bg="#ffffff")
                        pizzaName.place(x=125,y=20)

                        rs = Label(orderframe,text="Rs.",font=pizzaDes,bg="#ffffff")
                        rs.place(x=125,y=60)
                                
                        pizzaPrice = Label(orderframe,text=all_price[lselection],font=pizzaDes,bg="#ffffff")
                        pizzaPrice.place(x=155,y=60)

                        qty = Label(orderframe,text="Qty:",font=pizzaDes,bg="#ffffff")
                        qty.place(x=345,y=60)

                        pizzaqty = Label(orderframe,text=all_qty[lselection],font=pizzaDes,bg="#ffffff")
                        pizzaqty.place(x=385,y=60)

                        lselection=lselection+1

                        order1 = Label(orderframe,image=vegorder,bd=0)
                        order1.place(x=x1,y=y1)
                            
                        fy=fy+110 # Orders Frame Space In Y Position

                    cartCanvas.create_window(0, 0, anchor='nw', window=cartFrame)
                    cartCanvas.update_idletasks()
                    cartCanvas.configure(scrollregion=cartCanvas.bbox('all'), yscrollcommand=vsb.set)
                    cartCanvas.pack(side='left')
                    vsb.pack(fill='y', side='right')

                    size_sum=sum(all_price)
                    topping_sum=sum(all_toppings)
                    
                    

                    inc=1
                    subTotal=0
                    for st in range (1,ol1size+1):
                        ans=all_price[inc]*all_qty[inc]
                        subTotal=ans+subTotal
                        inc=inc+1

                    final=subTotal+topping_sum
                    convert3=str(final)
                    addP=convert3+"/-"

                    tax=(final*18)/100
                    convert4=str(tax)
                    addT=convert4+"/-"

                    qty_sum=sum(all_qty[1:])

                    grandTotal=final+tax
                    convert5=str(grandTotal)
                    addG=convert5+"/-"

                    

                    billFrame = Frame(hide,height=350,width=450,bg="#333333",bd=5,relief=RIDGE)
                    billFrame.place(x=650,y=20)
                    
                    bill = Label(billFrame,text="Bill",bg="#333333",fg="#ffffff",font=Troboto)
                    bill.place(x=170,y=8)

                    subT = Label(billFrame,text="Subtotal",bg="#333333",fg="#ffffff",font=vproboto)
                    subT.place(x=35,y=90)

                    subT_value = Label(billFrame,text=addP,bg="#333333",fg="#ffffff",font=vproboto)
                    subT_value.place(x=330,y=90)

                    quantity = Label(billFrame,text="Quantity",bg="#333333",fg="#ffffff",font=vproboto)
                    quantity.place(x=35,y=150)

                    quantity_value = Label(billFrame,text=qty_sum,bg="#333333",fg="#ffffff",font=vproboto)
                    quantity_value.place(x=330,y=150)

                    tax = Label(billFrame,text="Tax & Charges",bg="#333333",fg="#ffffff",font=vproboto)
                    tax.place(x=35,y=210)

                    tax_value = Label(billFrame,text=addT,bg="#333333",fg="#ffffff",font=vproboto)
                    tax_value.place(x=330,y=210)

                    lineimg = Label(billFrame,image=line,bd=0)
                    lineimg.place(x=18,y=250)

                    grandT = Label(billFrame,text="Grand Total",bg="#333333",fg="#ffffff",font=vproboto)
                    grandT.place(x=35,y=290)

                    grandT_value = Label(billFrame,text=addG,bg="#333333",fg="#ffffff",font=vproboto)
                    grandT_value.place(x=330,y=290)

                    mail = Label(hide,text="Enter Email-Id",bg="#555353",fg="#ffffff",font=toppings)
                    mail.place(x=650,y=410)

                    mailE1 = Entry(hide,bg="#ffffff",font=("Calibri",15),width=25)
                    mailE1.place(x=653,y=450)


                    def mailFunc():
                        mailId=mailE1.get()
                        hide.destroy()
                        cartCanvas.destroy()
                        vsb.destroy()
                        viewCart.destroy()
                        orderBack.destroy()
                        logout.destroy()
                        doneL = Label(root,image=doneImg,bg="#555353")
                        doneL.place(x=475,y=130)

                        placed=Label(root,text="Order Placed",bg="#555353",fg="#ffffff",font=PizzaTitle)
                        placed.place(x=505,y=440)

                        Ddb=MySQLdb.connect("localhost","root","","pizza")
                        Dcursor = Ddb.cursor()

                        try:
                            Dq="delete from pizzaDetail where pno != 1"
                            Dcursor.execute(Dq)
                            Ddb.commit()
                        except Exception as delEx:
                            Ddb.rollback()
                            print(delEx)
                        Ddb.close()
                        if(mailId==""):
                            pass
                        else:
                            sender='quizeeofficial@gmail.com'
                            convertQty=str(qty_sum)
                            message="Your Bill is: "+"\n\nSub Total: "+addP+"\nQuantity: "+convertQty+"\nTax & Charges: "+addT+"\nGrand Total: "+addG
                            print(message)
                            server = smtplib.SMTP('smtp.gmail.com',587)
                            server.starttls()
                            server.login("quizeeofficial@gmail.com","Quizee@python")
                            server.sendmail(sender,mailId,message)
                            server.quit()

                        def homeFunc():
                            doneL.destroy()
                            placed.destroy()
                            home.destroy()
                            menu()

                        home = Button(root,
                                      text="Menu",
                                      image=menuImg,
                                      compound=LEFT,
                                      bg="#6C9BEA",
                                      fg="#ffffff",
                                      font=vproboto,
                                      padx=5,
                                      pady=5,
                                      relief=FLAT,
                                      width=150,
                                      command=homeFunc
                                      )
                        home.place(x=555,y=500)
                    

                    sentMail = Button(hide,
                                      bg="#6C9BEA",
                                      fg="#ffffff",
                                      font=vproboto,
                                      padx=5,
                                      pady=5,
                                      text="Submit",
                                      width=10,
                                      command=mailFunc
                                      )
                    sentMail.place(x=653,y=495)

                    if ol1size == 0:
                        sentMail.config(state=DISABLED)
                    
                    def back():
                        hide.destroy()
                        orderBack.destroy()
                        cartCanvas.destroy()
                        vsb.destroy()
                        menu()
                        
                        
                    orderBack=Button(root,
                                     text="Back",
                                     command=back,
                                     image=vpizzabackImg,
                                     compound=LEFT,
                                     bg="#ffffff",
                                     fg="#000000",
                                     font=vproboto,
                                     relief=RAISED,
                                     padx=5,
                                     pady=5,
                                     width=160)
                    orderBack.place(x=1080,y=590)

                def logoutFunc():
                    vegPizzaBtn.destroy()
                    nonPizzaBtn.destroy()
                    dessertBtn.destroy()
                    viewCart.destroy()
                    logout.destroy()
                    login()

                viewCart = Button(root,
                                  text="View Cart",
                                  fg="#ffffff",
                                  bg="#417DE4",
                                  font=vproboto,
                                  padx=10,
                                  pady=5,
                                  image=view,
                                  compound=LEFT,
                                  command=viewCartFunc
                                  )
                viewCart.place(x=550,y=580)

                logout = Button(root,
                                text="Logout",
                                bg="#ffffff",
                                fg="#000000",
                                relief=RAISED,
                                font=vproboto,
                                compound=LEFT,
                                image=loimg,
                                padx=10,
                                command=logoutFunc
                                )
                logout.place(x=30,y=585)
                                                    
                
            def check():
                clid = le1.get()
                clpass = le2.get()

                if(clid and clpass) == "":
                    messagebox.showerror("Pizza Station","Enter All the fields")
                elif clid=="admin23" and clpass == "dharmesh":
                    root.destroy()
                    import Admin
                elif clid=="admin23" and clpass != "dharmesh":
                    messagebox.showerror("Pizza Station","Wrong Password")
                else:
                    try:
                        db=MySQLdb.connect("localhost","root","","pizza")
                        c1=db.cursor()

                        q1="select * from employee"
                        c1.execute(q1)
                        ldata=c1.fetchall()
                        lempid=[]
                        lpass=[]
                        for login in ldata:
                            lempid.insert(0,login[2])
                            lpass.insert(0,login[3])

                        if clid in lempid and clpass in lpass:
                            messagebox.showinfo("Pizza Station","Sucessful")
                            menu()
                            
                        elif clid in lempid and clpass not in lpass:
                            messagebox.showerror("Pizza Station","Wrong Password")
                            le2.delete(0,END)
                            
                        else:
                            db.rollback()
                            messagebox.showerror("Pizza Station","Error!!")
                            le1.delete(0,END)
                            le2.delete(0,END)

                        db.close()
                    except:
                        messagebox.showwarning("Pizza Station","Please Connect To database")

            lsubmit = Button(root,
                             text="Login",
                             fg="#ffffff",
                             bg="#6C9BEA",
                             width="15",
                             font=toppings,
                             command=check)
            lsubmit.place(x=833,y=450) # Login Submit button


        mb = Button(root,
                    image=img1,
                    height=660,
                    width=1280,
                    relief=SOLID,
                    border=0
                    )
        mb.pack()


        start = Button(root,
                       text="Start",
                       width=10,
                       relief=RIDGE,
                       bg="#daab61",
                       font=toppings,
                       command=login,
                       fg="#000000",
                       bd=5
                       )
        start.place(x=320,y=450)    
obj=PizzaSystem
obj.main()
root.mainloop()