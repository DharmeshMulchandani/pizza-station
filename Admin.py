from tkinter import *
from tkinter.font import Font
from tkinter import messagebox
import os,os.path
import re
import MySQLdb
import smtplib
import random


root = Tk()

root.title("Pizza Station")
root.config(bg="#555353")
root.iconbitmap('images/pizza_logo.ico')

w = 1280
h = 660
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

x = (screen_width/2) - (w/2)
y = (screen_height/2) - (h/2)

root.geometry("%dx%d+%d+%d" % (w,h,x,y))
root.resizable(False,False)


Tfont=Font(family="Roboto",
          size=25,
          weight="bold")
Loutfont=Font(family="Roboto",
          size=18,
          weight="bold")

adminFont=Font(family="Roboto",
          size=36,
          weight="bold")

Regfont=Font(family="Roboto",
             size=30,
             weight="bold")

datafont=Font(family="Roboto",
          size=14,
          weight="bold")


adminImg = PhotoImage(file='images/admin.png')
img2 = PhotoImage(file='images/reg1.png')
loimg = PhotoImage(file='images/logout.png')
backimg = PhotoImage(file='images/back.png')
empDetailsImg = PhotoImage(file='images/info.png')
fireEmpImg = PhotoImage(file='images/fired.png')
addEmpImg = PhotoImage(file='images/add-account.png')
tsImg = PhotoImage(file='images/financial.png')
rankImg = PhotoImage(file='images/ranking.png')

fireBg = PhotoImage(file='images/fireEmployee.png')
firelogo = PhotoImage(file='images/fired-logo.png')

avatar1 = PhotoImage(file='images/avatar.png')
avatar2 = PhotoImage(file='images/avatar2.png')

addEmpBtn = PhotoImage(file='images/addbtnemp.png')
fireEmpBtn = PhotoImage(file='images/fireEmpBtn.png')
infoEmpBtn = PhotoImage(file='images/infoEmpbtn.png')
salesBtn = PhotoImage(file='images/saleEmpBtn.png')
logoutBtn = PhotoImage(file='images/logoutBtn.png')

regbtn = PhotoImage(file='images/regSubmitButton.png')
clearbtn = PhotoImage(file='images/clearbtn.png')

def adminMain():

    adminFrame = Frame(root,bg="#ffffff",width=310,height=660)
    adminFrame.place(x=0,y=0)

    adminLogo = Label(adminFrame,image=adminImg,bd=0)
    adminLogo.place(x=94,y=23)

    adminName = Label(adminFrame,text="Admin",font=adminFont,bg="#ffffff",fg="#000000")
    adminName.place(x=80,y=156)


    def addEmployee():
        
        addEmpFrame = Frame(root,bg="#555353",height=660,width=980)
        addEmpFrame.place(x=310,y=0)

        regBg = Label(addEmpFrame,image=img2)
        regBg.place(x=0,y=0)

        regTitle = Label(addEmpFrame,text="Register",font=Regfont,bg="#000000",fg="#ffffff")
        regTitle.place(x=550,y=80)

        fullname = Label(addEmpFrame,text="Full Name",font=Loutfont,bg="#000000",fg="#ffffff")
        fullname.place(x=550,y=150)
        rege1 = Entry(addEmpFrame,fg="#000000",bg="#ffffff",width=25,font=("Calibri",15))
        rege1.place(x=555,y=190)
        rege1.focus()

        eid = Label(addEmpFrame,text="Email ID",font=Loutfont,bg="#000000",fg="#ffffff")
        eid.place(x=550,y=240)
        rege2 = Entry(addEmpFrame,fg="#000000",bg="#ffffff",width=25,font=("Calibri",15))
        rege2.place(x=555,y=280)

        empid = Label(addEmpFrame,text="Employee ID",font=Loutfont,bg="#000000",fg="#ffffff")
        empid.place(x=550,y=330)
        rege3 = Entry(addEmpFrame,fg="#000000",bg="#ffffff",width=25,font=("Calibri",15))
        rege3.place(x=555,y=370)

        rpass = Label(addEmpFrame,text="Password",font=Loutfont,bg="#000000",fg="#ffffff")
        rpass.place(x=550,y=420)
        rege4 = Entry(addEmpFrame,fg="#000000",bg="#ffffff",width=25,font=("Calibri",15),show="*")
        rege4.place(x=555,y=460)

        def clearFunc():
            rege1.delete(0,END)
            rege2.delete(0,END)
            rege3.delete(0,END)
            rege4.delete(0,END)
            rege1.focus()

        def check():
            chk_fname=rege1.get()
            chk_mail=rege2.get()
            chk_emp=rege3.get()
            chk_epass=rege4.get()

            chkdb = MySQLdb.Connect("localhost","root","","pizza")
            chkc = chkdb.cursor()

            chk_empid = [] 

            try:
                chkq="select * from employee"
                chkc.execute(chkq)
                chkdata=chkc.fetchall()
                for chk in chkdata:
                    chk_empid.append(chk[2])
                chkdb.commit()
            except Exception as chkex:
                pass
            chkdb.close()

            epass_len=len(chk_epass)
            match=re.findall("@gmail.com",chk_mail)
            if (chk_fname and chk_mail and chk_emp and chk_epass) == "":
                messagebox.showerror("Pizza Station","Enter all values")

            elif match==[]:
                messagebox.showerror("Pizza Station","Enter Mail Properly")
                rege2.focus()

            elif epass_len<8:
                messagebox.showerror("Pizza Station","Password minimum length should be 8")
                rege4.focus()

            elif chk_emp in chk_empid:
                messagebox.showerror("Pizza Station","This Employee Id already exist")
                rege3.focus()

            else:
                db=MySQLdb.Connect("localhost","root","","pizza")
                c1=db.cursor()

                try:
                    ins="insert into employee values('"+chk_fname+"','"+chk_mail+"','"+chk_emp+"','"+chk_epass+"')"
                    c1.execute(ins)
                    db.commit()
                    messagebox.showinfo("Pizza Station","Registered Sucessfully")                    
                except:
                    messagebox.showerror("Pizza Station","You are adding some wrong inputs pls try again!!!")
                    db.rollback()
                db.close()

                sender='quizeeofficial@gmail.com'
                message="Welcome "+chk_fname+" to Pizza Station"+"\nyour Id is: "+chk_emp+"\nyour password is: "+chk_epass
                server = smtplib.SMTP('smtp.gmail.com',587)
                server.starttls()
                server.login("quizeeofficial@gmail.com","Quizee@python")
                server.sendmail(sender,chk_mail,message)
                server.quit()
                
                clearFunc()

                #check function ends here 
                

        regsubmit = Button(addEmpFrame,
                           command=check,
                           image=regbtn,
                           relief=SOLID,
                           bd=0,
                           bg="#6C9BEA"
                           )
        regsubmit.place(x=555,y=500) 

        clear = Button(addEmpFrame,
                       font=Tfont,
                       command=clearFunc,
                       bd=0,
                       image=clearbtn
                       )
        clear.place(x=707,y=500)

    addEmp = Button(adminFrame,
                    relief=SOLID,
                    image=addEmpBtn,
                    command=addEmployee,
                    bd=0,
                    bg="#235196"
                    )
    addEmp.place(x=-1,y=228)

#----------------------------------------------------------------Add Employee Ends here

    def fireEmpFunc():

        fireEmpFrame = Frame(root,bg="#555353",height=660,width=980)
        fireEmpFrame.place(x=310,y=0)
        
        bg = Label(fireEmpFrame,image=fireBg,bd=0)
        bg.place(x=0,y=0)

        eidF = Entry(fireEmpFrame,fg="#000000",bg="#ffffff",width=25,font=("Calibri",15))
        eidF.place(x=493,y=360)
        eidF.focus()

        def removeEmp():
            e1 = eidF.get()

            chkdb = MySQLdb.Connect("localhost","root","","pizza")
            chkc = chkdb.cursor()

            chk_empid = [] 

            try:
                chkq="select * from employee"
                chkc.execute(chkq)
                chkdata=chkc.fetchall()
                for chk in chkdata:
                    chk_empid.append(chk[2])
                chkdb.commit()
            except Exception as chkex:
                pass
            chkdb.close()

            fdb = MySQLdb.connect("localhost","root","","pizza")
            fc = fdb.cursor()

            ans = messagebox.askyesno("Pizza Station","Are you sure you want to fire employee")
            
            if e1 in chk_empid:
                if ans == True:
                    try:
                        allq="select mail from employee where empid = %s"
                        fc.execute(allq,(e1,))
                        alldata=fc.fetchone()
                        for i in alldata:
                            pass
                        sender='quizeeofficial@gmail.com'
                        message="You are Fired from Pizza Station"
                        server = smtplib.SMTP('smtp.gmail.com',587)
                        server.starttls()
                        server.login("quizeeofficial@gmail.com","Quizee@python")
                        server.sendmail(sender,i,message)
                        server.quit()
                        fdb.commit()
                    except Exception as Eex:
                        print(Eex)
                        fdb.rollback()
                        
                    try:
                        fq="delete from employee where employeeId = %s"
                        fdata=(e1,)
                        fc.execute(fq,fdata)
                        fdb.commit()
                        messagebox.showinfo("Pizza Station","Employee Fired")
                    except Exception as ex:
                        print(ex)
                        fdb.rollback()
                    fdb.close()
                    eidF.delete(0,END)
                else:
                    messagebox.showwarning("Pizza Station","Process terminated")
            else:
                messagebox.showwarning("Pizza Station","Employee Id does not exist")

        def fbackFunc():
            fireEmpFrame.destroy()
            adminMain()

        fback = Button(fireEmpFrame,
                       text="Back",
                       fg="#000000",
                       bg="#ffffff",
                       font=Loutfont,
                       command=fbackFunc,
                       image=backimg,
                       compound=LEFT,
                       padx=10
                       )
        fback.place(x=830,y=580)
                
                    

        fButton = Button(fireEmpFrame,
                         text="Fire",
                         bg="#568BE7",
                         fg="#ffffff",
                         font=Loutfont,
                         image=firelogo,
                         compound=LEFT,
                         relief=FLAT,
                         padx=5,
                         width=100,
                         command=removeEmp
                         )
        fButton.place(x=493,y=400)

    delEmp = Button(adminFrame,
                    relief=SOLID,
                    image=fireEmpBtn,
                    command=fireEmpFunc,
                    bg="#235196",
                    bd=0
                    )

    delEmp.place(x=0,y=298)

    
#----------------------------------------------------------------Fire Employee Ends here

    def detailInfo():

        diFrame = Frame(root,height=660,width=1290,bg="lightgreen")
        diFrame.place(x=310,y=0)

        diFdata=[]
        diMdata=[]
        diIdata=[]
        
        xvalue=10
        yvalue=10

        imageList = [avatar1,avatar2]

        nameSelection=0

        didb = MySQLdb.connect("localhost","root","","pizza")
        dic = didb.cursor()
        try:
            diq = "select * from employee"
            dic.execute(diq)
            didata = dic.fetchall()
            for di in didata:
                diFdata.append(di[0])
                diMdata.append(di[1])
                diIdata.append(di[2])
            didb.commit()
        except Exception as diex:
            print(diex)
            didb.rollback()
        didb.close()

        di_container_size = len(diFdata)

        for cc in range(1,di_container_size+1):
            infoFrame = Frame(diFrame,width=350,height=100,bg="#ffffff")
            infoFrame.place(x=xvalue,y=yvalue)

            fullname = Label(infoFrame,text=diFdata[nameSelection],bg="#ffffff",font=datafont)
            fullname.place(x=80,y=10)

            mail = Label(infoFrame,text=diMdata[nameSelection],bg="#ffffff")
            mail.place(x=80,y=40)

            eid = Label(infoFrame,text="ID:",bg="#ffffff")
            eid.place(x=15,y=75)

            empid = Label(infoFrame,text=diIdata[nameSelection],bg="#ffffff")
            empid.place(x=30,y=75)

            profile = Label(infoFrame,image=imageList[1],bg="#ffffff")
            profile.place(x=5,y=5)

            yvalue = yvalue+110
            nameSelection=nameSelection+1

            if(cc>=6):
                xvalue=380

            if(yvalue>=660):
                yvalue=10

    

    detailEmp = Button(adminFrame,
                       relief=SOLID,
                       image=infoEmpBtn,
                       bd=0,
                       bg="#235196",
                       command=detailInfo
                       )
    detailEmp.place(x=0,y=368)

#--------------------------------------------------------------------Employee Info ends here

    Ts = Button(adminFrame,
                bg="#235196",
                relief=SOLID,
                image=salesBtn,
                bd=0
                )
    Ts.place(x=0,y=438)


    def logoutFunc():
        root.destroy()
        import PizaaStation

    logoutbtn = Button(adminFrame,
                       bg="#235196",
                       relief=SOLID,
                       image=logoutBtn,
                       bd=0,
                       command=logoutFunc
                       )
    logoutbtn.place(x=0,y=508)
                  
    
adminMain()
root.mainloop()
